---
author: Mireille Coilhac
title: Kit de survie GitLab en ligne
---

🌵 Comment créer mon contenu sur mon beau site tout neuf ?



!!! danger "Attention"

    * Ce tutoriel est destiné aux grands débutants, ne connaisssant pas du tout GitLab.
    * Il ne présente que le strict minimum pour être autonome dans la création d'un site.


!!! info "Travailler en ligne"

	L'IDE utilisée en ligne est VS Code Web.

    ![web IDE](images/web_ide_5.png){ width=90% }


!!! danger "Attention : noms de répertoires et noms de fichiers"

	Eviter les accents, caractères spéciaux, espaces. Pour séparer les éléments de nommage, utiliser les underscores :  
	Par exemple : écrire `chapitre_intro`

	

## I. Ajouter un répertoire dans docs

![web IDE](images/new_folder.png){ width=80% }

![web IDE](images/repertoire.png){ width=60% }


## II. Ajouter un répertoire dans un autre

Vous aurez souvent besoin d'ajouter d'autres répertoires comme :

* `images`
* `a_telecharger`
* `scripts`

![nouveau répertoire](images/nouv_repert.png){ width=80% }

![rens](images/rens_rep.png){ width=60% }


## III. Créer en ligne un fichier .md ou .py

!!! info "Editeur de texte intégré pour créer des fichiers `.md` ou `.py`"

	Sur le répertoire désiré, faire un clic droit, puis sélectionner **New File**

	![nouveau fichier](images/nouveau_fichier.png){ width=40% }

	Saisir le nom du fichier que vous désirez créer, sans oublier son extension : par exemple `cours_1.md`. 

	![nom du fichier](images/nom_fichier.png){ width=40% }

	Appuyer sur la touche "entrée" <kbd>↵</kbd> du clavier pour créer le fichier. Vous pouvez maintenant écrire dans la partie éditeur, à droite.

	![ecrire](images/ecrire.png){ width=40% }

	😊 Ce procédé est utilisable aussi pour écrire un fichier `.py` par exemple.

    👉 Vous pouvez aussi travailler hors ligne et utiliser un éditeur de texte comme Notepad++  pour créer vos fichiers `.md`, puis les télécharger ensuite dans votre dépôt.


!!! info "Comment écrire vos pages"

    👀 Vous pouvez explorer toutes les autres rubriques de ce tutoriel ...  
	😊 Bonne lecture ...



## IV. Télécharger un fichier

Vous pourrez ainsi télécharger vos fichiers `.md`, `.py`, `.jpg`, `.ipynb`, etc.

![nouveau fichier](images/upload.png){ width=80% }


## V. Faire un commit pour un nouveau répertoire, le téléchargement de fichiers, la création d'un fichier

!!! danger "Attention"

    Pour qu'une modification soit réellement effectuée (création de répertoire, modification de fichier ...) il faut faire un commit.

    On peut réliser un seul commit pour plusieurs actions effectuées.

!!! danger "⏳ Attention ... savoir patienter entre deux commits ..."

    La construction du site après un commit prend quelques instants. Il est vivement conseillé de vérifier sur le rendu du site que les modifications ont été prises en compte, **avant** de se lancer dans un nouveau commit. Il est parfois nécessaire de rafraichir plusieurs fois la page de rendu du site pour voir les modifications réalisées.   


!!! info "Commit pour nouveau répertoire, téléchargement fichier"

	!!! danger "Attention"

		Il **ne faut pas** faire un commit pour un répertoire qui a été créé, mais est encore vide.  
		
		😨 Ce répertoire serait **supprimé** après le commit.

	Sur la barre d'outil latérale gauche :

	![pour commit](images/pour_commit.png){ width=20% }

	![texte commit](images/texte_commit.png){ width=60% }

	![branche](images/sur_main.png){ width=90% }

	![retour projet](images/retour_projet.png){ width=95% }

    Au début, la construction est en cours :

    ![pipeline attend](images/en_cours.png){ width=95% }

    Une fois la construction terminée, on obtient : 

    ![pipeline ok](images/ok.png){ width=90% }

    Rafraichir la page du rendu de votre site. Il faut attendre un peu, et parfois recommencer, pour voir apparaître les modifications.
	

???+ note "Les commits"

    Un commit, c’est la sauvegarde des modifications, accompagnée d'un commentaire qui résume les modifications effectuées.



## VI. Modifier un fichier


!!! info "Modifier un fichier"

	![a_modifier](images/a_modif.png){ width=90% }

	Réaliser les modifications

	![modifs](images/modifs.png){ width=90% }



!!! info "Commit pour modification de fichier"

	On procède comme précédemment.

	![pour commit](images/pour_commit.png){ width=20% }


	Observer les modifications

	![visu modifs](images/visu_3.png){ width=80% }

	Si les modifications conviennent

	![faire push](images/faire_push.png){ width=80% }

	Procéder ensuite comme au IV.


## VII. Renommer un fichier

![renommer](images/renommer.png){ width=80% }

!!! danger "Attention"

    Si vous avez renommé un fichier (ou un répertoire) qui était écrit dans un fichier .pages, modifier ce fichier .pages avec le nouveau nom, **avant** de réaliser le commit.  



## VIII. L'interface Web IDE et le dépôt

Pour revenir sur le dépôt (⚠ attention ne pas oublier de faire un commit s'il y a eu une modification, avant de revenir sur le dépôt)

En bas à gauche :

![pour depot 1](images/pour_depot_1.png){ width=20% }

Puis en haut :

![pour depot 2](images/pour_depot_2.png){ width=30% }











