---
author: Mireille Coilhac
title: Erreurs fréquentes
---

🌵 Si vous rencontrez des problèmes ...

??? tip "Le rendu n'est pas bon"

    Vous voyez du code html ou markdown s'afficher.

    * Vérifier qu'il n'y a pas d'erreur d'indentation :
    Il n'en faut pas moins, mais pas plus non plus. Bien regarder les modèles proposés dans ce tutoriel.

    * Vérifier qu'une ligne vide nécessaire a bien été mise : avant un tableau, avant une liste à puce ...

    Si vous avez écrit du code entre     
    
    ````markdown 
    ```python
    du code python
    ```
    ````

    vérifier que vous avez bien mis 3 backticks au début et à la fin.

??? tip "Après un commit, cela ne passe pas à la coche verte **réussi** "

    👉 Attendre, puis rafraichir la page dans votre navigateur.

??? tip "La pipeline est en échec"

    * Vérifier qu'il n'y a pas d'erreur dans le fichier `.pages` (suite à des modifications ou suppressions de fichiers par exemple, 
    ou à une faute d'orthographe)

    * Vérifier qu'il n'y a pas d'erreur de syntaxe (indentation par exemple) dans le fichier `mkdocs.yml`

    👉 Si vous avez rectifié une erreur, après un nouveau `commit`, la pipeline devrait passer.


??? tip "L'IDE d'un exercice reste vide, alors que vous avez bien mis un fichier Python"

    * Si c'est votre premier exercice, attendre, et rafraichir la page dans le navigateur.

    * Vérifiez que l'IDE est bien dans une admonition, et qu'il n'y en a pas une autre imbriquée dans la même.

    * Vérifier que vous n'avez pas appelé deux fois le même fichier Python. Si vous voulez le faire (pour des méthodes différentes par exemple), 
    il faut créer un autre fichier, avec un nom différent, et un contenu légèrement différent (un `#` supplémentaire suffit)

    * Vérifier les noms de fichiers : ne pas avoir le sujet `.md` et l'exercice `.py` qui ont des noms identiques aux extensions près :
    Par exemple ne pas choisir `mon_exo.md` pour le sujet, et `mon_exo.py` pour l'énoncé qui doit apparaître dans l'IDE. Vous pouvez choisir par exemple
     `mon_exo_sujet.md` et `mon_exo.py`

    * Vérifier le chemin du fichier devant se trouver dans l'IDE : par exemple, est-il dans un dossier `scripts` ou pas ? 

    * Vérifier les paramètres du projet : 

    👉 Aller sur le dépôt, puis dans Déploiement, puis dans Pages

    ![Aller dans deploiement > pages](../08_tuto_fork\images\trouver_pages.png){ width=20% }

    👉 Il faut  **décocher** "Utiliser un domaine unique" 

    ![Décocher puis enregistrer](../08_tuto_fork/images/decocher_enregistrer.png){ width=75% }

    👉 Ne pas oublier de rafraichir la page du rendu.


??? tip "Le dépôt semble correct, mais le rendu ne correspond pas"

    👉 Essayer dans un autre navigateur. Si cela fonctionne bien, vider la mémoire cache de votre navigateur. Vous trouverez facilement en ligne comment procéder.

??? tip "J'ai créé un répertoire, il a disparu"

    👉 Si vous avez créé un nouveau répertoire sans y mettre de fichier dedans, il a disparu après le commit. Recommencer l'opération en ajoutant un fichier dedans.