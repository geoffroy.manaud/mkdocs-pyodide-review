---
author: Mireille Coilhac
title: Ajouts sur ce site
---

Voici les ajouts notables réaisés sur ce site depuis sa publication.

* 25/6/2023 : Les graphiques en Python [Lien vers les graphiques en Python](bibs_graphiques/bib_images.md)

* 1/7/2023 : Créer un fichier pdf [Lien vers Créer un fichier pdf](pdf/creer_pdf.md)

* 28/7/2023 : Lien pour permettre le téléchargement d'un fichier mis sur le site [Lien vers téléchargement de fichier : lire le III. 3.](02_basique/2_page_basique.md)

* 29/7/2023 : Ajout de cette rubrique sur les ajouts

* 30/7/2023 : Modification avancée du site [Modification avancée de votre site : lire le III.](08_tuto_fork/1_fork_projet.md)

* 1/8/2023 : Modifier le logo du site [Modification du logo: lire la fin du II.](08_tuto_fork/1_fork_projet.md)

* 1/9/2023 : Ajout du code à copier pour certains QCM [Une page basique : V. QCM ](02_basique/2_page_basique.md)

* 3/9/2023 : Ajout d'une méthode pour ouvrir directement dans basthon un fichier enregistré sur le site [iframe basthon : voir le II.](bibs_graphiques/bib_images.md)

* 5/9/2023 : Mettre des trous dans du texte [Une page basique I. 7](02_basique/2_page_basique.md)

* 26/10/2023 : Les erreurs fréquentes [Erreurs fréquentes](erreurs/erreurs_frequentes.md)

* 27/10/2023 : Surligner du code [Une page basique : lire le IX.](02_basique/2_page_basique.md)

* 08/12/2023 : Ajout du lien vers Pandoc en ligne : [pdf](pdf/creer_pdf.md)
