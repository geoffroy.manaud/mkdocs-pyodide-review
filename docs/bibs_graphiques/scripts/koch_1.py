from turtle import *

def koch(etape, cote):
    if etape == 0:
        forward(cote)
    else:
        koch(etape-1, cote/3)
        left(60)
        koch(etape-1, cote/3)
        right(120)
        koch(etape-1, cote/3)
        left(60)
        koch(etape-1, cote/3)


# hideturtle()  # on cache la tortue
# speed(0) # tortue rapide
koch(3, 200)
done()

