---
author: Mireille Coilhac
title: Intégrer basthon - Images  en Python
---

!!! info "Pas de bibliothèque graphique"

    La version actuelle de ce site ne permet pas dans un IDE d'appeler les bibliothèques graphiques comme turtle, matplotlib etc.

    ⏳ Une évolution ultérieure de Pyodide le permettra sûrement.


!!! info "Utiliser quand même des bibliothèques graphiques"

    Une façon de remédier à ce problème, est de ne pas utiliser l'IDE intégré, mais un "iframe" (souvent traduit par cadre en ligne)

    ⏳ Attention, l'affichage des images peut prendre quelques instants.  
    Pour l'utilisation en console Python, il faut cliquer sur l'icône ![image_prete](images/image_prete.png){ width=5% } lorsqu'elle clignote.

       
## I. Méthode utilisant le lien de partage copié sur le site basthon 

!!! warning "Remarque"

    Cette méthode n'est pas pratique pour des url très longues.
    La méthode suivante (II. II. Méthode utilisant un fichier enregistré) est plus pertinante dans ces cas-là, mais nécessite d'avoir enregistré le fichier sur votre  site.


!!! info "Les codes Python avec appel de bilbliothèque graphique"

    1. Ecrire le code Python :

        * Pour un simple éditeur Python : [éditeur basthon](https://console.basthon.fr/){ .md-button target="_blank" rel="noopener" }
        * Pour un notebook jupyter : [notebook jupyter basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

    2. Récupérer le code sous forme d'url en cliquant sur l'icône "Partager" : ![partager](images/partager.png){ width=2% }

    3. Incorporer l'url dans un "iframe" entre `src=` et `width="..." height="..." `

    4. Personnaliser les valeurs de `width=` et de `height=` qui correspondent respectivement à la largeur et à la hauteur du cadre ("iframe")

!!! info "Exemple de modèle de code"

    ````markdown title="Modèle de code à copier et compléter"
    <div class="centre">
    <iframe 
    src=
    url à coller ici
    width="..." height="..." 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
    ````


!!! info "Exemple avec turtle"

    ````markdown title="Code à copier"
    <div class="centre">
    <iframe 
    src=
    https://console.basthon.fr/?script=eJxLK8rPVSgpLSrJSVXIzC3ILypR0OLlyshMSYUIamjyciVnFiUDWYYGBkBOQWpeaQFIND2_JF_D1EAHLpySX56HohxDtS5pyg10THGpNdAxJkG9gY4uWDmSdohASn4e0IcAaadINQ
    width="900" height="500" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
    ````

<div class="centre">
<iframe 
src=
https://console.basthon.fr/?script=eJxLK8rPVSgpLSrJSVXIzC3ILypR0OLlyshMSYUIamjyciVnFiUDWYYGBkBOQWpeaQFIND2_JF_D1EAHLpySX56HohxDtS5pyg10THGpNdAxJkG9gY4uWDmSdohASn4e0IcAaadINQ
width="900" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>



!!! info "Exemple avec un notebook jupyter"
    
    ````markdown title="Code à copier"    
    <div class="centre">
    <iframe 
    src=
    https://notebook.basthon.fr/?ipynb=eJzFVMtu2zAQ_JUFc5EKpfUjvQjwP-TQnqrAYMlVTIRaEnw0NQx_T5Hv8I91GbuxHCBAmjaJTtwdamYwXHIjFFobRfttIwZMUsskRbvZNvf9ZVp7FK0YZLjR7pZEI6LLQZXelyB3vxxFyATRDN4ipGAkXVv8KLbNCZ1IIceEWrS8wEfkymkcE_fBDZBySMzIvC4k-NBRR70LYMAQBBbBal63HQF_3L-VQVezyaTedyz2qZrOSjlIQ9Y5X9UsgT9R5WQcLZXLlERL2dpGuJx8TiWDq0e-nxHD12SsifsckL2QKgIQdncqh2h-IHjeytFIhQG8zdEgAw9ZxVfJSmMPg0umr5RL2ADVcEjL9ECwWMD0T-MQ4Wm0Y3CccWGrT6FR2KVEGxFa-Avuo89PM3YK5zCtX6Le0dnKaNynUdUAZ8AHoaRaIVgJicPJyJuiR9TVpGZ832Nnnn_sqPDDAniQXmlYHu6MNn2_uwtI6X9Nwvi0p_88-C-Wnr2f9Pz9pC_eVvrpCR6b-vy2pp4ncNWcCNxgIH4xPKpSkRwKk1-nlaM5E2oTvZXr5QG4vAegIJavTJbXx-1iy_7oOz8Vg2Tli2OxHAy5INrZ9jff2FnB
    width="900" height="900" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
    ````

<div class="centre">
<iframe 
src=
https://notebook.basthon.fr/?ipynb=eJzFVMtu2zAQ_JUFc5EKpfUjvQjwP-TQnqrAYMlVTIRaEnw0NQx_T5Hv8I91GbuxHCBAmjaJTtwdamYwXHIjFFobRfttIwZMUsskRbvZNvf9ZVp7FK0YZLjR7pZEI6LLQZXelyB3vxxFyATRDN4ipGAkXVv8KLbNCZ1IIceEWrS8wEfkymkcE_fBDZBySMzIvC4k-NBRR70LYMAQBBbBal63HQF_3L-VQVezyaTedyz2qZrOSjlIQ9Y5X9UsgT9R5WQcLZXLlERL2dpGuJx8TiWDq0e-nxHD12SsifsckL2QKgIQdncqh2h-IHjeytFIhQG8zdEgAw9ZxVfJSmMPg0umr5RL2ADVcEjL9ECwWMD0T-MQ4Wm0Y3CccWGrT6FR2KVEGxFa-Avuo89PM3YK5zCtX6Le0dnKaNynUdUAZ8AHoaRaIVgJicPJyJuiR9TVpGZ832Nnnn_sqPDDAniQXmlYHu6MNn2_uwtI6X9Nwvi0p_88-C-Wnr2f9Pz9pC_eVvrpCR6b-vy2pp4ncNWcCNxgIH4xPKpSkRwKk1-nlaM5E2oTvZXr5QG4vAegIJavTJbXx-1iy_7oOz8Vg2Tli2OxHAy5INrZ9jff2FnB
width="900" height="900" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

!!! info "Exemple avec matplotlib"

    ````markdown title="Code à copier" 
    <div class="centre">
    <iframe 
    src=
    https://console.basthon.fr/?script=eJxNi0sKxCAQRPeCd6ilBpFJ9nMYEwIR_DSjIfbto4TA7KpevfKR8q8inZEYriCRFP5h0VUKuQa_WuKRxk6hSiFFw7erNvhUyG27-hjMWgruuGGasAypu3b8VDNg_dfZoOnXKEe-lL4B49spiw
    width="900" height="400" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
    ````


<div class="centre">
<iframe 
src=
https://console.basthon.fr/?script=eJxNi0sKxCAQRPeCd6ilBpFJ9nMYEwIR_DSjIfbto4TA7KpevfKR8q8inZEYriCRFP5h0VUKuQa_WuKRxk6hSiFFw7erNvhUyG27-hjMWgruuGGasAypu3b8VDNg_dfZoOnXKEe-lL4B49spiw
width="900" height="400" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

!!! info "Exemple avec p5"

    Une fois l'image réalisée, vous pourrez déplacer la souris dessus et y déplacer des rectangles.

    ````markdown title="Code à copier" 
        <div class="centre">
        <iframe 
        src=
        https://console.basthon.fr/?script=eJxLK8rPVSgwVcjMLcgvKlHQ4uXi5UpJTVMoTi0pLdDQtOLlUgCC5KLUxJJU58S8ssRiDRMDAx0FIKEJU5tSlFgOV5qWmZOjYWRqqgnhFqUml2jk5pcWp0boKIDpSB0FUwMQBusvKs3T0AQAfQUi4A
        width="900" height="500" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
        </div>
    ````

<div class="centre">
<iframe 
src=
https://console.basthon.fr/?script=eJxLK8rPVSgwVcjMLcgvKlHQ4uXi5UpJTVMoTi0pLdDQtOLlUgCC5KLUxJJU58S8ssRiDRMDAx0FIKEJU5tSlFgOV5qWmZOjYWRqqgnhFqUml2jk5pcWp0boKIDpSB0FUwMQBusvKs3T0AQAfQUi4A
width="900" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


## II. Méthode utilisant un fichier enregistré

!!! info "Exemple de modèle de code"

    Après avoir mis le fichier `mon_fichier.py` dans le dossier `scripts` de votre page

    ````markdown title="Code à copier"
    {% raw %}  
    <div class="centre">
    <iframe 
    src="https://console.basthon.fr/?from={{ page.canonical_url }}../scripts/mon_fichier.py"
    width="..." height="..." 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
    {% endraw %}
    ````

!!! info "Exemple avec turtle"

    ````markdown title="Code à copier"
    {% raw %}      
    <div class="centre" markdown="span">
    <iframe 
    src="https://console.basthon.fr/?from={{ page.canonical_url }}../scripts/koch_1.py"
    width="900" height="600" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
    {% endraw %}
    ````


<div class="centre" markdown="span">
<iframe 
src="https://console.basthon.fr/?from={{ page.canonical_url }}../scripts/koch_1.py"
width="900" height="600" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

!!! info "Exemple de modèle de code avec un notebook sur basthon"

    Après avoir mis le fichier `mon_fichier.ipynb` dans le dossier `scripts` de votre page

    ````markdown title="Modèle de code à copier et compléter"
    {% raw %}  
    <div class="centre">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../scripts/mon_fichier.ipynb"
    width="..." height="..." 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
    {% endraw %}
    ````

!!! info "Exemple avec turtle"

    ````markdown title="Code à copier"
    {% raw %}      
    <div class="centre" markdown="span">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../scripts/les_triangles.ipynb"
    width="900" height="600" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
    {% endraw %}
    ````

<div class="centre" markdown="span">
<iframe 
src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../scripts/les_triangles.ipynb"
width="900" height="600" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


!!! info "Ressources"

    Visiter : [Galerie basthon](https://basthon.fr/galerie.html){ .md-button target="_blank" rel="noopener" }

