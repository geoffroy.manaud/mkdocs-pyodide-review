# Tutoriels et aides pour l'enseignant qui crée son site

## 👉 Les sites modèles

Ce tutoriel accompagne les modèles suivants à cloner, pour pour aider à leur prise en main :

### Site avec des exercices interactifs en Python

[Rendu du site avec pyodide](https://modeles-projets.forge.apps.education.fr/mkdocs-pyodide-review/){ .md-button target="_blank" rel="noopener" }   

[Dépôt du site avec pyodide à cloner](https://forge.apps.education.fr/modeles-projets/mkdocs-pyodide-review){ .md-button target="_blank" rel="noopener" }  

### Site simple

[Rendu du site simple](https://modeles-projets.forge.apps.education.fr/mkdocs-simple-review/){ .md-button target="_blank" rel="noopener" }  

[Dépôt du site simple à cloner](https://forge.apps.education.fr/modeles-projets/mkdocs-simple-review){ .md-button target="_blank" rel="noopener" } 


## 👉 Cloner les sites modèles pour les personnaliser

Vous pouvez faire un «fork» (C'est à dire "cloner" ou "Créer une divergence") de ces modèles pour réaliser le vôtre.

Le tutoriel pour réaliser ce fork est sur ce site, dans la rubrique "Comment créer un site à partir d'un autre".

Lien direct : [Faire un "fork"](https://tutoriels.forge.apps.education.fr/mkdocs-pyodide-review/08_tuto_fork/1_fork_projet/){ .md-button target="_blank" rel="noopener" }



## 👉 Si vous débutez et pour comprendre la structure

Il est conseillé de commencer par : [Avant de démarrer](https://tutoriels.forge.apps.education.fr/mkdocs-pyodide-review/01_demarrage/1_demarrage/){ .md-button target="_blank" rel="noopener" }


!!! abstract "En bref"

    😀 Vous pourrez facilement recopier toutes les syntaxes depuis les différents tutoriels proposés.

    La version de pyodide utilisée est pour le moment la 0.9.1

    Le dépôt est ici : [Dépôt du tutoriel](https://forge.apps.education.fr/tutoriels/mkdocs-pyodide-review){ .md-button target="_blank" rel="noopener" }


_Dernière mise à jour le 08/12/2023_
