---
author: Mireille Coilhac
title: Ajouter des tags
---

## I. Dans votre dépôt

!!! info "A vérifier dans le dépôt"

    Si ce n'est pas déjà fait il faut : 

    **1. Créer une page tags.md, puis la mettre dans le dossier `docs`**

    !!! info "Exemple de fichier tags.md"

    	```markdown title="fichier tags.md"
    	# 🏷️ Tags

    	```


    **2. Modifier le fichier mkdocs.yml :**

    Sous `plugins`  ajouter si nécessaire : 

    ```text title="Dans le fichier mkdocs.yml sous plugins recopier :"
    - tags:
        tags_file: tags.md
    ```

## II. Les pages avec tags


Il suffit de compléter l'en-tête de la page `.md`


```markdown title="début du fichier `.md` à recopier"
---
author: Noms d'auteurs
title: Titre de la page
tags:
  - Nom du tag 
---

Suite de ma page ...

```

### Exemple dans "Chapitre 2" dans la page "Une fonction" 

Il y a le tag 2-fonction : [page Une fonction](https://modeles-projets.forge.aeif.fr/mkdocs-pyodide-review/02_Chapitre_2/2_fonction_2/){:target="_blank" }

!!! info "Fichiers utilisés pour la page : Une fonction "

    ````markdown title="2_fonction_2.md"
    ---
    author: Votre nom
    title: Une fonction
    tags:
      - 2-fonction
    ---

    La fonction `addition` prend en paramètres deux nombres entiers ou flottants, et renvoie la somme des deux.

    ???+ question "Compléter ci-dessous"
        {% raw %}
        {{ IDE('scripts/addition') }}
        {% endraw %}
    ````

### Exemple dans "Le morse" 

Il y a les tags  3-dictionnaires et Difficulté** : [page Le morse](https://modeles-projets.forge.aeif.fr/mkdocs-pyodide-review/03_mini_projet_1/mini_projet/){:target="_blank" }

!!! info "Début du fichier utilisés pour la page : Le morse "

    ```markdown title="mini_projet.md"
    ---
    author: Votre nom
    title: Le morse
    tags:
      - 3-dictionnaires
      - Difficulté **
    ---

    suite du fichier ...
    ```

## III. Les tags dans le menu de votre site

!!! info "Les tags dans le menu de votre site "

    🪄 Aucune autre manipulation  n'est nécessaire. Dans le menu, en cliquant sur Tags ([Tags dans le menu du site modèle](https://modeles-projets.forge.aeif.fr/mkdocs-pyodide-review/tags/){:target="_blank" }) tous les tags apparaîtront de façon automatique.



