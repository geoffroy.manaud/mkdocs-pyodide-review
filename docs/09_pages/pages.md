---
author: Mireille Coilhac
title: Structure du site et Organisation du menu
---

!!! danger "Attention"

    Dans cette rubrique nous allons voir comment utiliser un fichier `.pages`.

    👉 Pour éviter tout problème d'échec de construction du site, il est très fortement recommandé de ne créer ces fichiers `.pages` qu'**après** avoir réalisé les `commit`[^1] correspondant aux fichiers `.md` ou répertoires qui y sont notés.


## I. Organisation automatique du menu

!!! info "1. Présentation automatique"

    Le menu est présenté de façon automatique dans l'ordre alphabétique des répertoires du dépôt.

    Par exemple, l'arborescence suivante : 
    ```markdown title="arborescence"
    docs/
    ├── 01_chapitre_1
    ├── 02_chapitre_2
    ├── 03_chapitre_3
    ├── 04_chapitre_4
    ```

    produira sur le site le menu suivant :

    <div class="result" markdown>
    01_chapitre_1  
    02_chapitre_2  
    03_chapitre_3  
    04_chapitre_4  
    </div>


!!! info "2. Cas d'un seul fichier `.md` dans un répertoire"

    Si un répertoire ne contient qu'un seul fichier en `.md`, le nom du répertoire sera remplacé par le `title` du fichier en `.md`.

    Par exemple si 01_chapitre_1 ne contient que le fichier `.md` suivant:

    ```markdown title="fichier chapitre_1.md"
    ---
    author: Mon Nom
    title: Mon premier chapitre
    ---

    Suite ...
    ```
    Le menu qui apparaîtra sera le suivant :

    <div class="result" markdown>
    Mon premier chapitre  
    02_chapitre_2  
    03_chapitre_3  
    04_chapitre_4  
    </div>

## II. Modifier l'ordre des pages

!!! info "1. Par renommage des répertoires"

    Une solution consiste à nommer les répertoires par ordre alphabétique du menu désiré.   
    Cette solution peut se révéler fastidieuse lorsque l'on doit intercaler un chapitre, ce qui peut nécessiter beaucoup de renommages.


!!! info "En utilisant un fichier `.pages` "

    On peut ajouter un fichier `.pages` dans dans le répertoire docs  

    ```markdown title="arborescence"
    docs/
    ├── 01_chapitre_1
    ├── 02_chapitre_2
    ├── 03_chapitre_3
    ├── 04_chapitre_4
    ├── .pages
    ```

    ````markdown title="Code à copier pour le fichier `.pages`"
    arrange:
        - index.md
        - 03_chapitre_3
        - 02_chapitre_2
        - 04_chapitre_4
        - 01_chapitre_1
    ````

    <div class="result" markdown>
    Accueil   
    03_chapitre_3   
    02_chapitre_2   
    04_chapitre_4  
    01_chapitre_1  
    </div>

## III. Organiser un chapitre et renommer un chapitre


!!! info "1. Organisation du site modèle"

    ![orga modèle](images/orga_modele.png){ width=50% }

    Rendu : 

    ![rendu](images/rendu.png){ width=20% }


!!! info "2. Renommage d'un chapitre"

    ![chap 2](images/chap_2.png){ width=50% }

    👉 Ce chapitre étant composé de plusieurs fichiers `.md`, nous avons choisi de remplacer le titre par défaut 

    * " 02_chapitre_2 " 
    * par " Chapitre 2 - Python " 

    ````markdown title="Code à copier pour le fichier `.pages`"
    title: Chapitre 2 - Python
    ````

    Les titres qui apparaîssent lorsque l'on développe " Chapitre 2 - Python " sont ceux indiqués en `title` des fichiers `.md`

    ![rendu_chap_2.png](images/rendu_chap_2.png){ width=25% }

    👉 Changer l'ordre

    Nous pouvons également facilement changer l'ordre des pages de ce chapitre en complétant le fichier `.pages`

    ````markdown title="Code à copier pour le fichier `.pages`"
    title: Chapitre 2 - Python
    arrange:
        - 1_intro_python.md
        - 3_listes.md
        - 2_fonctions.md
    ````

## IV. Bilan sur les fichiers `.pages`

!!! abstract "Les fichiers `.pages`"

    Nous pouvons mettre des fichiers `.pages` dans docs ou dans différents répertoires.

    * Il donne le titre s'il commence par `title`
    * Il donne l'ordre des pages. Cela permet d'organiser des pages `.md`, et également des répertoires comme présenté ici :

    ````markdown title="Code exemple à copier pour un fichier `.pages`"
    title: Mon titre
    arrange:
        - page1.md
        - page2.md
        - repertoire_1
        - page3.md
        - repertoire_2
    ````

[^1]: Voir le tutoriel : [Kit de survie GitLab](https://tutoriels.forge.aeif.fr/mkdocs-pyodide-review/10_survie/kit_gitlab/
){ .md-button target="_blank" rel="noopener" }

