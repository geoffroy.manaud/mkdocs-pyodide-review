---
author: Mireille Coilhac
title: Avant de démarrer
---

!!! info "Pour utiliser ces tutos"           

    * Le langage utilisé est le [Markdown](https://fr.wikipedia.org/wiki/Markdown) (saveur mkdocs)
    * Nous utiliserons un vocabulaire spécifique présenté ici.
    * Le code peut être copié/collé facilement

## I. Pour démarrer

### Le langage Markdown

Le langage utilisé pour écrire le site est le Markdown. 

La syntaxe est très simple. Vous en verrez une présentation ici : 

[Documentation Markdown](https://www.markdownguide.org/cheat-sheet/){ .md-button target="_blank" rel="noopener" }

Pour composer vos documents, on peut écrire directement dans l'éditeur WEB IDE de la forge et utiliser le bouton «Créer un commit» comme un bouton sauvegarder. La différence par rapport à une sauvegarde «classique» est que vous êtes invités à commenter la ou les modifications apportées afin de créer un historique des modifications successives. Pour les utilisateurs avancés, on peut travailler localement, mais cela sera précisé dans un autre tutoriel.

### La structure du site

Le choix retenu pour ces modèles est de construire un site dont la page d'accueil est le contenu généré depuis le fichier `index.md` et les pages suivantes sont prises par ordre alphabétique de nom de dossier/fichier.

Ainsi, si la structure du répertoire `docs` est la suivante (comme dans le site modèle) :
```markdown title="Une partie de la structure du modèle"
docs/
├── 01_chapitre_1
│   └── chapitre_1.md
├── 02_chapitre_2
│   ├── scripts
│   ├── 1_intro_python.md
│   ├── 2_fonction.md
│   ├── 3_liste.md
│   └── .pages
├── index.md
└── tags.md
```
le site construit affichera comme page d'accueil le contenu provenant de la page `index.md`, et en menu les titres spécifiés au début des fichiers avec pour extension `.md` comme expliqué ci-dessous.

```markdown title="Code à copier au début d'un fichier .md"
---
author: compléter avec les noms d'auteurs
title: Compléter le titre qui sera affiché dans le menu
---
```

Si vous êtes amenés à séparer le contenu de votre chapitre en plusieurs pages, vous pouvez rajouter un fichier `.pages` qui contiendra le titre affiché au menu du site pour l'ensemble de ces pages.

Ici par exemple :

```markdown title="Code exemple d'un fichier .pages à recopier"
title: Chapitre 2 - Python
```
En l'absence de ce fichier `.pages` le menu du site affichera le nom du dossier, c'est à dire ici chapitre 2 (et non Chapitre 2 - Python).

Le rendu du site modèle correspondant à cette structure est le suivant : 

![accueil](images/rendu_accueil.png){ width=70%; : .center }





## II. Le vocabulaire

### Indentation

* Une indentation est un décalage vers la droite de 4 espaces (souvent réalisé automatiquement avec la touche tabulation du clavier)

* Un texte indenté est un texte pour lequel toutes les lignes sont indentées :

```markdown title="texte indenté"

    Ceci est un texte indenté.  
    Toutes les lignes sont décalées de 4 espaces vers la droite.  
    Il ne faut pas oublier les indentations lorsqu'elles sont nécessaires.  
    C'est souvent la cause de problèmes de rendu.
``` 
```markdown title="texte non indenté"

Ceci est un texte non indenté.  
Toutes les lignes commencent au début de la ligne.    
Il ne faut pas oublier les indentations lorsqu'elles sont nécessaires.  
C'est souvent la cause de problèmes de rendu.
``` 

### Backtick ou apostrophe inversée

Pour écrire un morceau de code dans un texte, Markdown l’identifie au moyen du caractère appelé le **Backtick** ou **apostrophe inversée** (`` ` ``). Attention, à ne pas le confondre avec les guillemets. On le trouve généralement avec la touche <kbd>ALT GR</kbd> + <kbd>è</kbd> du clavier.


### Les "admonitions"

Les « admonitions » sont les « boîtes » comme celles-ci. Elles peuvent s'imbriquer les unes dans les autres.


???+ question

    Résoudre cet exercice.
    Mon énoncé

    ??? tip "Astuce 1"

        Ma belle astuce 1

        ??? tip "Astuce 2"

            Ma belle astuce 2

            ??? tip "Astuce 3"

                Ma belle astuce 3

    ??? success "Solution"

        La solution


!!! warning "Remarque"

    texte de la remarque indenté


### Les "IDE"

Nous appelons IDE une fenêtre dans laquelle nous pouvons écrire du code.

{{ IDE('ide') }}

[Tutoriel sur les IDE](https://tutoriels.forge.aeif.fr/mkdocs-pyodide-review/03_ecrire_exos/exercices/){ .md-button target="_blank" rel="noopener" }



## III. Récupérer la syntaxe pour réaliser votre site

!!! info "Copier les syntaxes"
        
    Il suffit de cliquer en haut à droite de la syntaxe à recopier sur l'icône suivante ![copier](copier_coller.png){ width=5% }

    Vous n'aurez plus qu'à effectuer un "coller" dans le document de votre site.


## IV. Personnaliser la page d'accueil du site que vous avez cloné

La page d'accueil se trouve dans le fichier `index.md`du dossier `docs`.

😀 Il suffit de la personnaliser.
