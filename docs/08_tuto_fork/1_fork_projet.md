---
author: Mireille Coilhac
title: Créer votre propre site
---

# Comment créer un site à partir d'un autre ?

## I. Recopier le site

👉 Aller sur le dépôt du site qui sert de modèle.

[Site à copier pour cours à usage général](https://forge.apps.education.fr/modeles-projets/mkdocs-simple-review){ .md-button target="_blank" rel="noopener" } 
[Site à copier pour cours et exercices avec Python intégré](https://forge.apps.education.fr/modeles-projets/mkdocs-pyodide-review){ .md-button target="_blank" rel="noopener" } 


👉 On va recopier le site : Cliquer sur le bouton «Créer une divergence» en haut à droite («fork» en anglais)

![nom image](images/site_ref_2.png){ width=95% }

👉 Remplir le champ Project name, puis sélectionner le domaine avec le menu déroulant.

Cocher «Public» pour publier le projet puis cliquer sur «Fork project»

![nom image](images/remplir_2.png){ width=95% }

😀 On obtient : 

![nom image](images/new_2.png){ width=95% }

👉 Dans le menu de gauche sélectionner "Intégration et livraison continue" puis "Pipelines"

![pipeline](images/pipeline_2.png){ width=70% }

👉 Puis en haut à droite cliquer sur «Run pipeline»

![run pipeline2](images/run_pipeline_4.png){ width=95% }

👉 Puis cliquer à nouveau sur «Run pipeline» sans rien changer à ce qui est proposé

![run pipeline3](images/run_pipeline_3.png){ width=95% }

⌛ Etre assez patient et attendre un peu. On attend pendant qu'il s'affiche build.

![build](images/build.png){ width=50% }

😀 Après quelques instants s'affiche : 

![passed](images/passed_2.png){ width=50% }


👉 Vous pouvez enfin cliquer sur "Déploiement" puis sur "pages" 

![rendu](images/trouver_pages.png){ width=30% }


👉 Il faut  décocher "Utiliser un domaine unique" 

Vous obsrvez que l'url proposée est très compliquée. Elle sera simplifiée après avoir décoché "Utiliser un domaine unique" puis enregistré la modification

![Décocher puis enregistrer](images/decocher_enregistrer.png){ width=75% }


👉 Après avoir enregistré la modification, vous obtenez une url plus simple : 

![url simple](images/url_simple.png){ width=50% }

C'est l'adresse de rendu de votre site, vous pouvez la noter 😊.



## II. Personnaliser le site

!!! info "😀 C'est **presque** fini !"


!!! info "Il faut modifier le README.md "

    👉 Cliquer sur le nom de votre site en haut à gauche

    ![clic mon site](images/mon_site.png){ width=30% }

    👉 Cliquer sur "Modifier", puis sélectionner "EDI Web"

    ![web ide](images/web_ide_5.png){ width=95% }

    👉 Cliquer sur le fichier README.md 

    ![readme](images/readme_1.png){ width=40% }

    👉 Visualisation du fichier README.md 

    ![fichier readme](images/file_readme.png){ width=80% }

    👉 Modifier le fichier README.md 

    ![fichier readme](images/modif_readme.png){ width=60% }

    👉 Enregistrer la modification du fichier en réalisant un "commit" 

    Voir ci-dessous

!!! info "Réaliser un commit "

    Suivre cette méthode pour toutes les modifications de fichiers de votre site.

    Cliquer sur l'icône :

    ![commit](images/commit_1.png){ width=70% }

    Renseigner le commit :

    ![message commit](images/message_commit.png){ width=70% }

    Visualiser les modifications apportées par le commit demandé :

    ![visu](images/visu.png){ width=90% }

    Faire le commit :

    ![faire commit](images/faire_commit.png){ width=70% }

    En bas à droite (visible quelques instants), Cliquer sur Go to project pour visualiser l'évolution de la construction du site.

    ![retour projet](images/retour_projet.png){ width=90% }

    Au début, la construction est en cours :

    ![pipeline attend](images/en_cours.png){ width=90% }

    Une fois la construction terminée, on obtient : 

    ![pipeline ok](images/ok.png){ width=90% }

    Rafraichir la page du rendu de votre site. Il faut attendre un peu, et parfois recommencer, pour voir apparaître les modifications.



!!! warning "Personnaliser le projet : Modifier le fichier mkdocs.yml"

    👉 Revenir sur Web IDE

    ![web ide](images/web_ide_5.png){ width=90% }

    👉 Sélectionner le fichier mkdocs.yml

    ![fichier mkdocs.yml](images/mkdocs_1.png){ width=40% }

    👉 Modifier le fichier mkdocs.yml
    
    Modifier les clefs de configurations :

    - `site_name:`
    - `site_description:`
    - `copyright:`
    - `extra: social: link:` (celui avec le lien `mailto:`)
    - éventuellement `theme:`

    👉 Faire le commit

!!! info "Modifier le fichier index.md"

    👉 Revenir sur Web IDE

    ![web ide](images/web_IDE.png){ width=90% }

     👉 Sélectionner le fichier `index.md` du dossier `docs`

    ![index](images/index_1.png){ width=40% }

    * Éditer le fichier `index.md`
    * Le personnaliser. Il s'agit de la page d'accueil de votre site.

    Si besoin voir le tutoriel [Avant de démarrer](../01_demarrage/1_demarrage.md){:target="_blank" }


    👉 Faire le commit
    

!!! abstract "URL du site"

    😀 On peut se rendre sur l'adresse du rendu (nous l'avons trouvée dans "Déploiement" puis "Pages") pour voir le résultat.


!!! info "Changer de logo"

    Vous pouvez choisir un autre logo en suivant ce lien : [Logos Material Design Icons](https://pictogrammers.com/library/mdi/){ .md-button target="_blank" rel="noopener" }

    * Comme expliqué précédemment, ouvrir le fichier `mkdocs.yml`
    * Vers la ligne 25 vous pouvez lire :  
    `logo: material/stairs-up`
    
    👉 Il suffit de remplacer `stairs-up` par la référence choisie.

    Par exemple : 

    avec : `logo: material/teddy-bear` 
    
    on obtient le rendu suivant : 

    ![logo ours](images/ours.png){ width=30% }




## III. Votre propre contenu

Pour ajouter vos propres pages, voir le tutoriel [Kit de survie Gitlab en ligne](../10_survie/kit_gitlab.md){:target="_blank" }


??? tip "⚙️ Modification avancée de votre site"

    ⚠️ Cette modification n'est à réaliser que si vous avez bien réfléchi aux conséquences. 

    Elle **supprime** le lien direct de votre site 

    * vers le dépôt du site : ![forge](images/forge.png){ width=6% }

    * vers le dépôt de la page active : ![modif_page](images/modif_page.png){ width=5% }

    👉 Vous pourrez toujours atteindre votre dépôt à partir de son adresse url.

    👉 Ci-dessous votre haut de page avant et après la modification :

    Avant : ![avant](images/avant_modif.png){ width=30% } 
    
    Après : ![après](images/apres_modif.png){ width=30% }

	??? note "Comment faire ?"

    	* Suivre les explications du II. pour ouvrir le fichier `mkdocs.yml`

    	* Vers la ligne 53, repérer :  

    	`# Fin de modifications à effectuer, ne pas toucher les lignes suivantes sauf si vous comprenez ce que vous faites.`

    	Quelques lignes en dessous, mettre en commentaire (ajouter simplement `#` au début de la ligne) les deux lignes suivantes.

      	`# repo_url: !ENV [CI_PROJECT_URL]`  
		`# edit_uri: !ENV [EDIT_VARIABLE]`

        👉 Faire le commit
		

        
        !!! warning "Ne pas supprimer"

            Il est recommandé de **ne pas supprimer** ces deux lignes pour pouvoir les rétablir facilement en supprimant le `#` du début de la ligne.
		


