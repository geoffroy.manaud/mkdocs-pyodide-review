---
author: Mireille Coilhac et Vincent-Xavier Jumel
title: Une page basique
---

## I. Syntaxe basique de Markdown

!!! info "Les titres"

    ```markdown title="Le code à copier"
    ## Titre 1 : Mon titre de niveau 1

    Mon paragraphe

    ### Titre 2 : Mon titre de niveau 2
    ```
    <div class="result" markdown>
    ## Titre 1 : Mon titre de niveau 1

    Mon paragraphe 1

    ### Titre 2 : Mon titre de niveau 2

    Mon paragraphe 2
    </div>

### 2. Les tableaux

Il est facile de réaliser des tableaux avec Markdown, mais la présentation est basique.  
Les largeurs de colonnes sont gérées automatiquement, les espaces entre les `|  |` peuvent être aléatoires.  
Il faut laisser une ligne vide avant de commencer un tableau.


!!! info "Les tableaux"

    ```markdown title="Le code à copier"
    | Titre 1  | Titre 2 | Titre 3 |
    | :---    | :----:    | ---:   |
    | aligné gauche   | centré  | aligné droite |
    | Pomme | Brocoli | Pois |
    ```
    <div class="result" markdown>

    | Titre 1  | Titre 2 | Titre 3 |
    | :---    | :----:    | ---:   |
    | aligné gauche   | centré  | aligné droite |
    | Pomme | Brocoli | Pois |
    
    </div>

Pour des tableaux plus sophistiqués, on peut les écrire en HTML : 


!!! info "Les tableaux en HTML"
    ```html title="Le code à copier"
    <table>
    <tr>
        <td style="border:1px solid; font-weight:bold;">Poires</td>
        <td style="border:1px solid; font-weight:bold;">Pommes</td>
        <td style="border:1px solid; font-weight:bold;">Oranges</td>
        <td style="border:1px solid; font-weight:bold;">Kiwis</td>
        <td style="border:1px solid; font-weight:bold;">...</td>
    </tr>
    <tr>
        <td style="border:1px solid; font-weight:bold;">1 kg</td>
        <td style="border:1px solid; font-weight:bold;">2 kg</td>
        <td style="border:1px solid; font-weight:bold;">1 kg</td>
        <td style="border:1px solid; font-weight:bold;">500 g</td>
        <td style="border:1px solid; font-weight:bold;">...</td>
    </tr>
    </table>
    ```
    <div class="result" markdown>
    <table>
    <tr>
        <td style="border:1px solid; font-weight:bold;">Poires</td>
        <td style="border:1px solid; font-weight:bold;">Pommes</td>
        <td style="border:1px solid; font-weight:bold;">Oranges</td>
        <td style="border:1px solid; font-weight:bold;">Kiwis</td>
        <td style="border:1px solid; font-weight:bold;">...</td>
    </tr>
    <tr>
        <td style="border:1px solid; font-weight:bold;">1 kg</td>
        <td style="border:1px solid; font-weight:bold;">2 kg</td>
        <td style="border:1px solid; font-weight:bold;">1 kg</td>
        <td style="border:1px solid; font-weight:bold;">500 g</td>
        <td style="border:1px solid; font-weight:bold;">...</td>
    </tr>
    </table>
    </div>


### 3. Emojis qui peuvent être utiles


🌐 
👉
⚠️
🌵
💡
🤔
🌴
😂
🤿
😀
✏️
📝
😢
🐘
🖐️
👓
👗
👖
⌛
😊
🖲️
🏳️
😴
💻
👀

### 4. Insérer une image

A partir de Web IDE : Créer un dossier `images`. Télécharger dans ce dossier les images.

!!! info "Image à gauche"

    ```markdown title="Le code à copier"
    ![nom image](images/paysage_reduit.jpg){ width=20% }

    Par défaut l'image est placée à gauche.
    ```
    <div class="result" markdown>

    ![nom image](images/paysage_reduit.jpg){ width=20% }

    Par défaut l'image est placée à gauche.

    </div>


!!! info "Image à droite"

    ```markdown title="Aligner une image à droite"
    ![](images/paysage_reduit.jpg){ width=10%; align=right }

    Le texte se place **à gauche** de mon image placée à droite.
    ```
    <div class="result" markdown>

    ![](images/paysage_reduit.jpg){ width=10%; align=right }

    Le texte se place **à gauche** de mon image placée à droite.

    </div>

!!! info "3 images côtes à côtes"

    ```markdown title="Le code à copier"
    ![](images/paysage_reduit.jpg){ width=5% }
    ![](images/paysage_reduit.jpg){ width=10% }
    ![](images/paysage_reduit.jpg){ width=15% }
    ```
    <div class="result" markdown>

    ![](images/paysage_reduit.jpg){ width=5% }
    ![](images/paysage_reduit.jpg){ width=10% }
    ![](images/paysage_reduit.jpg){ width=15% }

    </div>

 
!!! info "Image centrée"

    ```markdown title="Le code à copier"
    ![paysage](images/paysage_reduit.jpg){ width=10%; : .center }

    Le texte se place **en dessous** de l'image centrée.
    ```
    <div class="result" markdown>

    ![paysage](images/paysage_reduit.jpg){ width=10%; : .center }

    Le texte se place **en dessous** de l'image centrée.

    </div>

### 5. Ecriture de code

!!! info "Les backticks"

    Nous utilisons pour cela un ou trois **backticks** (ou **apostrophe inversée**). Attention, à ne pas le confondre un **backtick** avec un guillemet. On le trouve généralement avec les touche <kbd>ALT GR</kbd> + <kbd>è</kbd> du clavier.


!!! info "Code au fil d'un texte inline"

    ```markdown title="Le code à copier"
    La variable `nombres` est de type `list`.
    ```
    <div class="result" markdown>

    La variable `nombres` est de type `list`.

    </div>

!!! info "Dans une console Python"

    ````markdown title="Le code à copier"
    ```pycon
    >>> nombres = [3, 8, 7]
    >>>fruits = ['Poire', 'Pomme', 'Orange', 'Kiwi']
    ```
    ````
    <div class="result" markdown>

    ```pycon
    >>> nombres = [3, 8, 7]
    >>> fruits = ['Poire', 'Pomme', 'Orange', 'Kiwi']
    ```
    </div>

!!! info "Dans un éditeur Python"

    ````markdown title="Le code à copier"
    ```python
    print("Hello World")
    ```
    ````
    <div class="result" markdown>

    ```python
    print("Hello World")
    ```
    </div>

!!! info "Dans un éditeur Python avec numéros de lignes"

    ````markdown title="Le code à copier"
    ```python linenums='1'
    a = 1
    b = 2
    t = b
    b = a
    a = t
    ```
    ````
    <div class="result" markdown>

    ```python linenums='1'
    a = 1
    b = 2
    t = b
    b = a
    a = t
    ```
    </div>

!!! info "Coloration syntaxique Python Inline"

    ```markdown title="Le code à copier"
    Nous allons utiliser le type `#!py list` de Python
    ```
    <div class="result" markdown>

    Nous allons utiliser le type `#!py list` de Python

    </div>

  
### 6. Les touches du clavier

!!! info "Faire apparaître des touches du clavier"

    ```markdown title="Le code à copier"
    Nous allons utiliser la touche <kbd>%</kbd>
    ```
    <div class="result" markdown>

    Nous allons utiliser la touche <kbd>%</kbd>

    </div>


### 7. Des "trous dans le texte"

!!! info "Les espaces"

    Pour faire apparaître des espaces dans un texte, mettre des espaces ne suffit pas .

    ```markdown title="Le code avec espace"
    Par exemple : `a == 3` est                 booléenne.
    ```
    <div class="result" markdown>

    Par exemple : `a == 3` est                 booléenne.

    </div>

!!! info "une possibilité pour ajouter des espaces"

    ```markdown title="Le code avec espace"
    Par exemple : `a == 3` est $\hspace{10em}$ booléenne.
    ```

    <div class="result" markdown>

    Par exemple : `a == 3` est $\hspace{10em}$ booléenne.

    </div>


## II. Les admonitions

Toutes les admonitions ont un titre par défaut. Pour personnaliser un titre, il suffit de le rajouter entre guillemets.


```markdown title="Admonition info"
!!! info "Mon info"
    
    Ma belle info qui doit être indentée
```
!!! info "Mon info"
    
    Ma belle info qui doit être indentée


```markdown title="Admonition remarque"
!!! warning "Remarque"

    Ma remarque qui doit être indentée
```
!!! warning "Remarque"

    Ma remarque qui doit être indentée


```markdown title="Admonition attention"
!!! danger "Attention"

    texte indenté   
```
!!! danger "Attention"

    texte indenté


```markdown title="Note à dérouler"
??? note "se déroule en cliquant dessus"

    Ma note indentée
```

??? note "se déroule en cliquant dessus"

    Ma note indentée


```markdown title="Astuce à dérouler"
??? tip "Astuce"

    Ma belle astuce indentée
```

??? tip "Astuce"

    Ma belle astuce indentée


```markdown title="Résumé"
!!! abstract "Résumé"

    Mon bilan indenté
```

!!! abstract "Résumé"

    Mon bilan indenté 


```markdown title="Note repliable dépliée"
???+ note dépliée

    Mon texte indenté
```

???+ note dépliée

    Mon texte indenté


```markdown title="Note à cliquer"
??? note pliée "Note à cliquer"

    Mon texte indenté qui se dévoilera après le clic  
```

??? note pliée "Note à cliquer"

    Mon texte indenté qui se dévoilera après le clic


```markdown title="Échec"
!!! failure "Echec"

    Mon texte indenté
```

!!! failure "Echec"

    Mon texte indenté



```markdown title="Bug"
!!! bug "Bug"

    Mon texte indenté
```

!!! bug

    Mon texte indenté


```markdown title="Exemple"
!!! example "Exemple"

    Mon exemple indenté
```

!!! example "Exemple"

    Mon exemple indenté


```markdown title="Note dans la marge gauche"
!!! note inline "Note à gauche"

    Texte de la note indenté
    
Le texte non indenté se place à droite de la note.  
Il est en dehors de l'admonition.  
Il doit être assez long  
...
```

!!! note inline "Note à gauche"

    Texte de la note indenté
    
Le texte non indenté se place à droite de la note.  
Il est en dehors de l'admonition.  
Il doit être assez long  
...


```markdown title="Note dans la marge droite"

!!! note inline end "Note à droite"

    Texte de la note indenté
    
Le texte non indenté se place à droite de la note.  
Il est en dehors de l'admonition.  
Il doit être assez long  
...
```

!!! note inline end "Note à droite"

    Texte de la note indenté
    
Le texte non indenté se place à droite de la note.  
Il est en dehors de l'admonition.  
Il doit être assez long  
...

!!! info "Panneaux coulissants"

    ```markdown title="Le code à copier"
    !!! example "Exemples avec trois panneaux"

        === "Panneau 1"
            Ici du texte concernant ce panneau 1

            Il peut prendre plusieurs lignes

        === "Panneau 2"
            Ici du texte concernant ce panneau 2

            Il peut prendre plusieurs lignes

        === "Panneau 3"
            Ici du texte concernant ce panneau 3

            Il peut prendre plusieurs lignes

            On peut aussi mettre une image : 

            ![nom image](images/paysage_reduit.jpg){ width=20% }
    ```  
    <div class="result" markdown>

    !!! example "Exemples avec trois panneaux"

        === "Panneau 1"
            Ici du texte concernant ce panneau 1

            Il peut prendre plusieurs lignes

        === "Panneau 2"
            Ici du texte concernant ce panneau 2

            Il peut prendre plusieurs lignes

        === "Panneau 3"
            Ici du texte concernant ce panneau 3

            Il peut prendre plusieurs lignes

            On peut aussi mettre une image : 

            ![nom image](images/paysage_reduit.jpg){ width=20% }


        </div>


## III. Faire des liens 

Ce qui est entre crochets comme `[link text]` est personnalisable.

### 1. Liens externes

!!! info "Lien externe sans bouton"

    ```markdown title="Le code à copier"
    [Catalogue de ressources](https://natb_nsi.forge.aeif.fr/ressources/)
    ```
    <div class="result" markdown>

    [Catalogue de ressources](https://natb_nsi.forge.aeif.fr/ressources/)

    </div>

!!! info "Lien externe sans bouton dans une autre fenêtre"

    ```markdown title="Le code à copier"
    [Catalogue de ressources](https://natb_nsi.forge.aeif.fr/ressources/){:target="_blank" }
    ```
    <div class="result" markdown>

    [Catalogue de ressources](https://natb_nsi.forge.aeif.fr/ressources/){:target="_blank" }

    </div>



!!! info "Lien externe avec bouton dans une autre fenêtre"

    ```markdown title="Le code à copier"
    [Catalogue de ressources](https://natb_nsi.forge.aeif.fr/ressources/){ .md-button target="_blank" rel="noopener" }
    ```
    <div class="result" markdown>

    [Catalogue de ressources](https://natb_nsi.forge.aeif.fr/ressources/){ .md-button target="_blank" rel="noopener" }

    </div>

### 2. Liens internes

!!! info "Lien vers un article du **même** répertoire"

    Lien vers le fichier `mon_article.md` du **même** répertoire.
 
    ```markdown title="Le code à copier"
    [link text](mon_article.md)
    ```
    <div class="result" markdown>

    [link text](mon_article.md)

    </div>

!!! info "Lien vers un article du **répertoire parent** du répertoire courant"

    Lien vers le fichier `mon_article.md`.

    ```markdown title="Le code à copier"
    [Mon lien](../mon_article.md)
    ```
    <div class="result" markdown>

    [Mon lien](../mon_article.md)

    </div>

!!! info "Lien vers un article d’un **sous-répertoire** du répertoire courant"

    Lien vers le fichier `mon_article.md` du sous-répertoire `directory`.

    ```markdown title="Le code à copier"
    [link text](directory/mon_article.md)
    ```
    <div class="result" markdown>

    [link text](directory/mon_article.md)

    </div>


👉 Le suivant est le seul qui fonctionne ici :

!!! info "Lien vers un article d’un **sous-répertoire du répertoire parent du répertoire actif**"

    Lien vers le fichier `exercices.md` du sous-répertoire `03_ecrire_exos` du répertoire parent du répertoire actif.


    ```markdown title="Le code à copier"
    [Ecrire des exercices](../03_ecrire_exos/exercices.md)
    ```
    <div class="result" markdown>

    [Ecrire des exercices](../03_ecrire_exos/exercices.md)

    </div>


### 3. Liens de téléchargements de fichiers

Mettre le fichier `file_telecharger` dans un dossier `a_telecharger`. Bien indiquer le bon chemin comme dans l'exemple ci-dessous

!!! info "Mon fichier à donner en téléchargement"

    ```markdown title="Le code à copier"
    
    🌐 Fichier à télécharger :

    Fichier `file_telecharger` : [Clic droit, puis "Enregistrer la cible du lien sous"](a_telecharger/file_telecharger)

    ```
    <div class="result" markdown>

    🌐 Fichier à télécharger :
    
    Fichier `file_telecharger` : [Clic droit, puis "Enregistrer la cible du lien sous"](a_telecharger/file_telecharger)

    </div>



## IV. Insertion de vidéo

Il suffit le plus souvent de recopier le code qui est proposé dans "partager" et "intégrer" de la vidéo que l'on veut mettre.

!!!example Vimeo
    ```html
    <div class="centre">
    <iframe 
    src="https://player.vimeo.com/video/138623558?color=b50067&title=0&byline=0&portrait=0" 
    width="640" height="360" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
    ```
    <div class="result" markdown>
    <div class="centre"><iframe src="https://player.vimeo.com/video/138623558?color=b50067&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div>
    </div>

!!!example Youtube
    ```html
    <iframe width="560" height="315" 
    src="https://www.youtube.com/embed/VnhBoQAgIVs" 
    title="YouTube video player" 
    frameborder="0" 
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
    </iframe>
    ```
    <div class="result" markdown>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/VnhBoQAgIVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    

## V. QCM

!!! info "Barrer des réponses fausses"

    Mettre un texte entre ~~ et ~~ permet de le barrer.

!!! info "Créer un QCM"

    ```markdown title="Le code à copier"
    ???+ question

        Voici diverses propositions

        === "Cocher la ou les affirmations correctes"
                
            - [ ] Proposition 1
            - [ ] Proposition 2
            - [ ] Proposition 3
            - [ ] Proposition 4

        === "Solution"
                
            - :x: ~~Proposition 1~~ Optionnel : Faux car ... 
            - :white_check_mark: Proposition 2 . Optionnel : Juste car ...
            - :white_check_mark: Proposition 3 . Optionnel : Juste car ...
            - :x: ~~Proposition 4~~ Optionnel : Faux car ... 
    ```
    <div class="result" markdown>

    ???+ question

        Voici diverses propositions

        === "Cocher la ou les affirmations correctes"
                
            - [ ] Proposition 1
            - [ ] Proposition 2
            - [ ] Proposition 3
            - [ ] Proposition 4

        === "Solution"
                
            - :x: ~~Proposition 1~~ Optionnel : Faux car ... 
            - :white_check_mark: Proposition 2 . Optionnel : Juste car ...
            - :white_check_mark: Proposition 3 . Optionnel : Juste car ...
            - :x: ~~Proposition 4~~ Optionnel : Faux car ... 

    </div>


!!! info "Créer un QCM avec une autre présentation sans score (Source Vincent Bouillot)"

    Cette présentation semble mieux fonctionner avec Chrome qu'avec Firefox.
    A chaque fois que l'on rafraichit la page l'ordre des questions est modifié si on a choisi `shuffle = True`

    ```markdown title="Le code à copier"
    Question 1 : Quelle est la réponse à la question universelle ? Cocher deux réponses.
    {% raw %}
    {{ qcm(["$6\\times 7$", "$\\int_0^{42} 1 dx$", "Je ne sais pas", "La réponse D"], [1,2], shuffle = True) }}
    {% endraw %}

    Question 2 : 1 + 1 = ? Cocher deux réponses.
    {% raw %}
    {{ qcm(["Je ne sais pas", "2", "L'âge du capitaine", "10 en binaire"], [2,4], shuffle = True) }}
    {% endraw %}
    ```

    <div class="result" markdown>

    Question 1 : Quelle est la réponse à la question universelle ? Cocher deux réponses.
    {{ qcm(["$6\\times 7$", "$\\int_0^{42} 1 dx$", "Je ne sais pas", "La réponse D"], [1,2], shuffle = True) }}
    
    Question 2 : 1 + 1 = ? Cocher deux réponses.
    {{ qcm(["Je ne sais pas", "2", "L'âge du capitaine", "10 en binaire"], [2,4], shuffle = True) }}
    
    </div>

    **Détails de la syntaxe :**

    * Argument 1: Tableau de strings contenant vos propositions.

    * Argument 2: Entier ou tableau d'entiers indiquant les bonnes réponses. L'indexation naturelle (1 à N) est choisie.

    * Argument 3: True pour une génération aléatoire à chaque rechargement du site web. False sinon. 


!!! info "Créer un QCM avec validation, rechargement et score (Source Vincent Bouillot)"

    Toutes les questions sont regroupées dans une liste de listes.
    
    🌵 Dans la version actuelle, le code Python n'est possible que dans les propositions de réponses, comme dans l'exemple ci-dessous, mais pas dans les questions.

    !!! warning "Ecriture de réponses en Python" 

        ⚠️ Comme dans l'exemple ci-dessous, pour une réponse qui commence par du code Python, laisser un espace tout de suite après le  `"`  du début.
        
        ```markdown title="Code correct"
        " `#!py meubles[1]` vaut `#!py 'Table'`"
        ```

        ```markdown title="Code incorrect"
        "`#!py meubles[1]` vaut `#!py 'Table'`"
        ```

    ```markdown title="Le code à copier"
    {% raw %}
    {{multi_qcm(
    ["Quelle est la réponse à la question universelle ? Cocher deux réponses.", 
    ["$6\\times 7$", "$\\int_0^{42} 1 dx$", "Je ne sais pas", " `#!py sum([i for i in range(10)])`", "La réponse D"], [1, 2]],
    ["1 + 1 = ? Cocher deux réponses.",
    ["Je ne sais pas", "2", "L'âge du capitaine", "10 en binaire"], [2, 4]]
    )}}
    {% endraw %}
    ```

    <div class="result" markdown>

    {{multi_qcm(
    ["Quelle est la réponse à la question universelle ? Cocher deux réponses.", 
    ["$6\\times 7$", "$\\int_0^{42} 1 dx$", "Je ne sais pas", " `#!py sum([i for i in range(10)])`", "La réponse D"], [1, 2]],
    ["1 + 1 = ? Cocher deux réponses.",
    ["Je ne sais pas", "2", "L'âge du capitaine", "10 en binaire"], [2, 4]]
    )}}

    </div>

!!! info "Créer un QCM avec validation, rechargement et score, réponses et commentaires"

    ```markdown title="Le code à copier"
    {% raw %}
    === "QCM"
      
        Cliquez sur vos propositions aux questions puis validez.
	 
	    {{multi_qcm(
        ["Quelle est la réponse à la question universelle ? Cocher deux réponses.", 
        ["$6\\times 7$", "$\\int_0^{42} 1 dx$", "Je ne sais pas", "La réponse D"], [1, 2]],
        ["1 + 1 = ? Cocher deux réponses.",
        ["Je ne sais pas", "2", "L'âge du capitaine", "10 en binaire"], [2, 4]]
        )}}


    === "Réponses"

        Cliquez sur les différentes propositions
          

        Question 1 : Quelle est la réponse à la question universelle ? Cocher deux réponses.
        {{ qcm(["$6\\times 7$", "$\\int_0^{42} 1 dx$", "Je ne sais pas", "La réponse D"], [1, 2], shuffle = True) }}
    
        Question 2 : 1 + 1 = ? Cocher deux réponses.
        {{ qcm(["Je ne sais pas", "2", "L'âge du capitaine", "10 en binaire"], [2, 4], shuffle = True) }}


    === "Commentaires"

        Lisez bien les remarques suivantes
        

        **Question 1 :** Commentaire 1

	    **Question 2 :** Commentaire 1

   
    {% endraw %}
    ```

    <div class="result" markdown>

    === "QCM"
      
        Cliquez sur vos propositions aux questions puis validez.
	 
	    {{multi_qcm(
        ["Quelle est la réponse à la question universelle ? Cocher deux réponses.", 
        ["$6\\times 7$", "$\\int_0^{42} 1 dx$", "Je ne sais pas", "La réponse D"], [1, 2]],
        ["1 + 1 = ? Cocher deux réponses.",
        ["Je ne sais pas", "2", "L'âge du capitaine", "10 en binaire"], [2, 4]]
        )}}


    === "Réponses"

        Cliquez sur les différentes propositions
          

        Question 1 : Quelle est la réponse à la question universelle ? Cocher deux réponses.
        {{ qcm(["$6\\times 7$", "$\\int_0^{42} 1 dx$", "Je ne sais pas", "La réponse D"], [1,2], shuffle = True) }}
    
        Question 2 : 1 + 1 = ? Cocher deux réponses.
        {{ qcm(["Je ne sais pas", "2", "L'âge du capitaine", "10 en binaire"], [2,4], shuffle = True) }}


    === "Commentaires"

        Lisez bien les remarques suivantes
        

        **Question 1 :** Commentaire 1

	    **Question 2 :** Commentaire 1

    </div>
        


!!! info "Utilisation avancée : Créer un QCM avec validation, rechargement et score avec un fichier csv"

    ⌛ 😊 Attendre un peu pour cette partie de tuto ...

    {{multi_qcm("scripts/exemple_qcm.csv")}}

    {{multi_qcm("exemple_qcm.csv")}}
    




## VI. Exercice suivi d'une solution qui se découvre en cliquant

### 1. Exercice sans code

!!! info "Exercice sans code Python"

    ```markdown title="Le code à copier"
    ???+ question "Mon exercice"

        **1.** Question 1.   
        Texte de la question indenté

        ??? success "Solution"

            Ici se trouve la solution rédigée de la question 1.
            Le texte doit être indenté dans cette nouvelle admonition

        **2.** Question 2.   
        Texte de la question indenté

        ??? success "Solution"

            Ici se trouve la solution rédigée de la question 2.
            Le texte doit être indenté dans cette nouvelle admonition
    ```
    <div class="result" markdown>

    ???+ question "Mon exercice"

        **1.** Question 1.   
        Texte de la question indenté

        ??? success "Solution"

            Ici se trouve la solution rédigée de la question 1.
            Le texte doit être indenté dans cette nouvelle admonition

        **2.** Question 2.   
        Texte de la question indenté

        ??? success "Solution"

            Ici se trouve la solution rédigée de la question 2.
            Le texte doit être indenté dans cette nouvelle admonition

    </div>

### 2. Exercice avec code : un tout premier exemple


!!! danger "Attention"

    Un exercice avec IDE doit absolument se trouver dans une admonition `???+ question` 


!!! info "Un IDE avec du code à compléter"

    ````markdown title="Le code à copier"
    ???+ question

        Compléter le script ci-dessous :
        Mettre le fichier python dans un dossier scripts

        {% raw %}
        {{ IDE('scripts/mon_fichier') }}
        {% endraw %}

    ??? success "Solution"
        On peut donner la solution sous forme de code à copier :
        ```python
        # Code de la solution
        ```       
    ````
    <div class="result" markdown>

    ???+ question

        Compléter le script ci-dessous :
        Mettre le fichier python dans un dossier scripts

        {{ IDE('scripts/mon_fichier') }}

    ??? success "Solution"
        On peut donner la solution sous forme de code à copier :
        ```python
        # Code de la solution
        ``` 
    </div>

!!! danger "Attention"

    Avec la version actuelle, une IDE doit-être dans une admonition, on ne peut pas, dans la même, la faire suivre d'une autre admonition.

### 3. D'autres exemples

Pour en savoir plus, voir la rubrique [Ecrire des exercices](../03_ecrire_exos/exercices.md/){ .md-button target="_blank" rel="noopener" }  

Vous y trouverez des exemples d'exercices avec des réponses dans des IDE, qui peuvent s'afficher après plusieurs tentatives de l'élève, et plein d'autres possibilités.
    
        
## VII. Notebooks pythons : sujet à télécharger et corrections cachées ou visibles

On peut évidemment mettre en ligne un Notebook Python, et faire travailler les élèves dessus.

Mettre le fichier `mon_fichier_sujet.ipynb` dans un dossier `a_telecharger`. Bien indiquer le bon chemin comme dans l'exemple ci-dessous

!!! info "Mon sujet sur notebook à télécharger avec correction cachée"

    ```markdown title="Le code à copier"
    ???+ question "Mon sujet sur notebook à télécharger avec correction cachée"

        Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

        🌐 TD à télécharger : Fichier `mon_fichier_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_sujet.ipynb)

        ⏳ La correction viendra bientôt ... 

    <!--- La correction à télécharger plus tard A SORTIR DE L'ADMONITION
    👉 Bien sortir ce commentaire de l'admonition en supprimant l'indentation
    🌐 Fichier `mon_fichier_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_corr.ipynb)
    -->
    ```
    <div class="result" markdown>

    ???+ question "Mon sujet sur notebook à télécharger avec correction cachée"

        Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

        🌐 TD à télécharger : Fichier `mon_fichier_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_sujet.ipynb)

        ⏳ La correction viendra bientôt ... 

    <!--- La correction à télécharger plus tard A SORTIR DE L'ADMONITION
    👉 Bien sortir ce commentaire de l'admonition en supprimant l'indentation
    🌐 Fichier `mon_fichier_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_corr.ipynb)
    -->

    </div>

Si l'on veut dévoiler la correction, il suffit de supprimer la syntaxe qui la cachait dans un commentaire.

!!! info "Mon sujet sur notebook à télécharger avec correction visible"

    ```markdown title="Le code à copier"
    ???+ question "Mon sujet sur notebook à télécharger avec correction visible"

        Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

        🌐 TD à télécharger : Fichier `mon_fichier_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_sujet.ipynb)

        🌐 Correction à télécharger : Fichier `mon_fichier_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_corr.ipynb)
    ```
    <div class="result" markdown>

    ???+ question "Mon sujet sur notebook à télécharger avec correction visible"

        Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

        🌐 TD à télécharger : Fichier `mon_fichier_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_sujet.ipynb)

        🌐 Correction à télécharger : Fichier `mon_fichier_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_corr.ipynb)

    </div>


## VIII. Exercices en lignes

Il existe beaucoup d'exercices en ligne, il suffit de donner le lien.

!!! info "Bouton de lien vers un exercice"

    ```markdown title="Le code à copier"
    ???+ question "Lectures dans deux tableaux (1) - non guidé"

        Les éléments d'un tableau sont-ils tous différents ?

        [Éléments tous différents](https://e-nsi.forge.aeif.fr/pratique/N1/420-tous_differents/sujet/){ .md-button target="_blank" rel="noopener" }
    ```
    <div class="result" markdown>

    ???+ question "Lectures dans deux tableaux (1) - non guidé"

        Les éléments d'un tableau sont-ils tous différents ?

        [Éléments tous différents](https://e-nsi.forge.aeif.fr/pratique/N1/420-tous_differents/sujet/){ .md-button target="_blank" rel="noopener" }

    </div>



## IX. Ecriture de code : compléments

!!! info "Annotations première présentation"

    !!! danger "Attention"

        Bien veiller à mettre une ligne vide après la balise fermante ```   et avant les annotations numérotées


    L'exemple suivant est donné avec Python, mais peut servir avec n'importe quel langage (avec ````html```` par exemple)

    ````markdown title="Le code à copier"
    ```python 
    note = float(input("Saisir votre note : "))
	if note >= 16:  # (1)
    	print("TB")
	elif note >= 14:  # (2)
    	print("B")
	elif note >= 12:  # (3)
    	print("AB")
	elif note >= 10:
    	print("reçu")
	else:
    	print("refusé")
    ```

    1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    2. :warning: `elif` signifie **sinon si**.

    3. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    !!! warning "Prenez le temps de lire les commentaires (cliquez sur les +)"
    ````
    <div class="result" markdown>

    ```python 
    note = float(input("Saisir votre note : "))
	if note >= 16:  # (1)
    	print("TB")
	elif note >= 14:  # (2)
    	print("B")
	elif note >= 12:  # (3)
    	print("AB")
	elif note >= 10:
    	print("reçu")
	else:
    	print("refusé")
    ```

    1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    2. :warning: `elif` signifie **sinon si**.

    3. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    !!! warning "Prenez le temps de lire les commentaires (cliquez sur les +)"

    </div>


!!! info "Annotations autre présentation"

    ````markdown title="Le code à copier"
    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +


    ```python
	    note = float(input("Saisir votre note : "))
	    if note >= 16:  # (1)
    	    print("TB")
	    elif note >= 14:  # (2)
    	    print("B")
	    elif note >= 12:  # (3)
    	    print("AB")
	    elif note >= 10:
    	    print("reçu")
	    else:
    	    print("refusé")
    ```

    1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    2. :warning: `elif` signifie **sinon si**.

    3. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.
    ````
    <div class="result" markdown>
    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +


    ```python
	    note = float(input("Saisir votre note : "))
	    if note >= 16:  # (1)
    	    print("TB")
	    elif note >= 14:  # (2)
    	    print("B")
	    elif note >= 12:  # (3)
    	    print("AB")
	    elif note >= 10:
    	    print("reçu")
	    else:
    	    print("refusé")
    ```

    1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    2. :warning: `elif` signifie **sinon si**.

    3. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.
    </div>

!!! info "Surlignage de lignes de code"

    Vous pouvez surligner des lignes, consécutives ou non, en couleur. 

    ````markdown title="Le code à copier"
    ```python hl_lines="1 3-5"
    def trouve_max(tableau):
        maximum = tableau[0]
        for valeur in tableau:
            if valeur > maximum:
                maximum = valeur
        return maximum
    ```
    ````

    <div class="result" markdown>
    ```python hl_lines="1 3-5"
    def trouve_max(tableau):
        maximum = tableau[0]
        for valeur in tableau:
            if valeur > maximum:
                maximum = valeur
        return maximum
    ```
    </div>

## X. Les arbres 

Il suffit de numéroter les noeuds : n0, n1, n2 etc. avec les étiquettes entre parenthèses, et les flèches pour les arcs entre les noeuds.
En 1er à gauche.

!!! info "Un arbre"

    ````markdown title="Le code à copier"
    ```mermaid
        %%{init: {'themeVariables': {'fontFamily': 'monospace'}}}%%
        flowchart TB
            n0(15) --> n1(6)
            n1 --> n3(1)
            n1 --> n4(10)
            n0 --> n2(30)
            n2 --> n8[Null]
            n2 --> n5(18)
            n5 --> n6(16)
            n5 --> n7(25)
    ```
    ````
    <div class="result" markdown>

    ```mermaid
        %%{init: {'themeVariables': {'fontFamily': 'monospace'}}}%%
        flowchart TB
            n0(15) --> n1(6)
            n1 --> n3(1)
            n1 --> n4(10)
            n0 --> n2(30)
            n2 --> n8[Null]
            n2 --> n5(18)
            n5 --> n6(16)
            n5 --> n7(25)
    ```
    </div>


!!! info "Arbres avec certains nœuds absents "

    Voir l'exemple qui suit :

    * On numérote l'arc à ne pas représenter (le 1er est le numéro 0), et on utilise par exemple pour le numéro 3 la syntaxe :  
    `linkStyle 3 stroke-width:0px;`
    * On met les nœuds vides avec `opacity:0` :  
    `style E opacity:0;`
    

!!! info "Des nœuds absents mais leur espace conservé"

    ````markdown title="Le code à copier"
    ```mermaid
    flowchart TD
        A(12) --- B(10)
        A --- C(15)
        B --- D(5)
        B --- E( )
        D --- F(4)
        D --- G(8)
        C --- H( )
        C --- I(20)
        linkStyle 3 stroke-width:0px;
        linkStyle 6 stroke-width:0px;
        style E opacity:0;
        style H opacity:0;
    ```
    ````
    <div class="result" markdown>

    ```mermaid
    flowchart TD
        A(12) --- B(10)
        A --- C(15)
        B --- D(5)
        B --- E( )
        D --- F(4)
        D --- G(8)
        C --- H( )
        C --- I(20)
        linkStyle 3 stroke-width:0px;
        linkStyle 6 stroke-width:0px;
        style E opacity:0;
        style H opacity:0;
    ```
    </div>

!!! info "Placement des nœuds définis"

    Dans le cas des arbres binaires de recherche par exemple, on a besoin que les nœuds soient représentés dans un ordre bien précis.
    Il suffit pour cela de les donner dans le code mermaid dans l'ordre de ce qui serait le parcours en largeur de l'arbre (par niveau).

    ````markdown title="Le code à copier"
    ```mermaid
    graph TD
    A(5)
    B(2)
    C(8)
    F( )
    G( )
    D(6)
    E(9)
    H( )
    I( )
    J( )
    K( )
    A --> B
    A --> C
    C --> D
    C --> E
    B --> F
    B --> G
    D --> H
    D --> I
    E --> J
    E --> K
    ```
    ````
    <div class="result" markdown>

    ```mermaid
    graph TD
    A(5)
    B(2)
    C(8)
    F( )
    G( )
    D(6)
    E(9)
    H( )
    I( )
    J( )
    K( )
    A --> B
    A --> C
    C --> D
    C --> E
    B --> F
    B --> G
    D --> H
    D --> I
    E --> J
    E --> K
    ```
    </div>



[lien vers la documentation mermaid](https://mermaid.live/edit#pako:eNpVkE1qw0AMha8itGohvoAXgcZus0lJodl5vBAeOTOk88NYpgTbd-84ptBqJfS-J6Q3YRc0Y4nXRNHApVYecr00lUl2EEdDC0Wxn48s4ILn-wyHp2OAwYQYrb8-b_xhhaCaTivGIMb627JJ1cN_9jxD3ZwoSojtX-XyHWZ4beyHyev_KyZxdr01PZU9FR0lqCi1uEPHyZHV-expNSgUw44VlrnV3NP4JQqVXzJKo4TPu--wlDTyDseoSbi2lB92v0PWVkJ635J4BLL8AM-3Wn4){ .md-button target="_blank" rel="noopener" }

!!! info "Un arbre horizontal"

    ````markdown title="Le code à copier"
    ```mermaid
    graph LR
	    A( )
	    B(0)
	    C(1)
	    D(0)
	    E(1)
	    F(0)
	    G(1)
	    A --- B
	    A --- C
	    B --- D
	    B --- E
	    C --- F
	    C --- G
    ```
    ````

    <div class="result" markdown>

    ```mermaid
    graph LR
	    A( )
	    B(0)
	    C(1)
	    D(0)
	    E(1)
	    F(0)
	    G(1)
	    A --- B
	    A --- C
	    B --- D
	    B --- E
	    C --- F
	    C --- G
    ```
    </div>


## XI. Les graphes

!!! info "graphe orienté"
    ````markdown title="Le code à copier"
    ``` mermaid
        graph LR
        A[texte 1] ----> |50| C[texte 2];
        A -->|1| B[B];
    ```
    ````
    <div class="result" markdown>

    ``` mermaid
        graph LR
        A[texte 1] ----> |50| C[texte 2];
        A -->|1| B[B];
    ```
    </div>


!!! info "graphe orienté horizontal"

    ````markdown title="Le code à copier"
    ``` mermaid
        graph LR
            A[Editeur de texte PHP] --> C[Serveur Apache]
            C --> B[Navigateur]
    ```
    ````
    <div class="result" markdown>

    ``` mermaid
        graph LR
            A[Editeur de texte PHP] --> C[Serveur Apache]
            C --> B[Navigateur]
    ```
    </div>


!!! info "graphe non orienté"

    ````markdown title="Le code à copier"
    ``` mermaid
        graph LR
            A ---|1| B
            B ---|5| C
            A ---|5| D
            B ---|1| D
            D ---|1| C
            A ---|50| C
    ```
    ````
    <div class="result" markdown>

    ``` mermaid
        graph LR
            A ---|1| B
            B ---|5| C
            A ---|5| D
            B ---|1| D
            D ---|1| C
            A ---|50| C
    ```
    </div>


[lien vers la documentation mermaid](https://mermaid.live/edit#pako:eNpVkE1qw0AMha8itGohvoAXgcZus0lJodl5vBAeOTOk88NYpgTbd-84ptBqJfS-J6Q3YRc0Y4nXRNHApVYecr00lUl2EEdDC0Wxn48s4ILn-wyHp2OAwYQYrb8-b_xhhaCaTivGIMb627JJ1cN_9jxD3ZwoSojtX-XyHWZ4beyHyev_KyZxdr01PZU9FR0lqCi1uEPHyZHV-expNSgUw44VlrnV3NP4JQqVXzJKo4TPu--wlDTyDseoSbi2lB92v0PWVkJ635J4BLL8AM-3Wn4){ .md-button target="_blank" rel="noopener" }


## XII. Les classes

!!! info "Une classe"

    ````markdown title="Le code à copier"
    ```mermaid
        classDiagram
        class Yaourt{
            str arome
            int duree
            str genre
            __init__(arome,duree)
            modifie_duree(duree)
            modifie_arome(arome)
        }
    ```
    ````
    <div class="result" markdown>

    ```mermaid
        classDiagram
        class Yaourt{
            str arome
            int duree
            str genre
            __init__(arome,duree)
            modifie_duree(duree)
            modifie_arome(arome)
        }
    ```
    </div>

[lien vers la documentation mermaid](https://mermaid.live/edit#pako:eNpVkE1qw0AMha8itGohvoAXgcZus0lJodl5vBAeOTOk88NYpgTbd-84ptBqJfS-J6Q3YRc0Y4nXRNHApVYecr00lUl2EEdDC0Wxn48s4ILn-wyHp2OAwYQYrb8-b_xhhaCaTivGIMb627JJ1cN_9jxD3ZwoSojtX-XyHWZ4beyHyev_KyZxdr01PZU9FR0lqCi1uEPHyZHV-expNSgUw44VlrnV3NP4JQqVXzJKo4TPu--wlDTyDseoSbi2lB92v0PWVkJ635J4BLL8AM-3Wn4){ .md-button target="_blank" rel="noopener" }




