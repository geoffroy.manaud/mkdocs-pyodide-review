---
author: Mireille Coilhac
title: Crédits
---

Le site est hébergé par la forge de [l'*Association des Enseignants d'Informatique de France*](https://aeif.fr/index.php/accueil/){:target="_blank" }.

Le site est construit avec [`MkDocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/){:target="_blank" }.

Les exercices intéractifs Python utilisent Pyodide développé pour MkDocs par Vincent Bouillot.

😀 Un grand merci à  Vincent-Xavier Jumel et Vincent Bouillot qui ont réalisé la partie technique de ce site, et qui m'ont beaucoup aidé pour les mises à jour. 
Merci également à Charles Poulmaire pour ses relectures attentives et ses conseils judicieux. Merci enfin à Romain Janvier qui m'aide à faire évoluer ce tutoriel, et trouve des nouveautés.

Le logo :material-magic-staff: fait partie de MkDocs sous la référence `magic-staff`  [https://pictogrammers.com/library/mdi/](https://pictogrammers.com/library/mdi/){:target="_blank" }

