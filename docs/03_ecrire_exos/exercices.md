---
author: Mireille Coilhac 
title: Ecrire des exercices
---

Nous verrons ici des exemples d'utilisation de Pyodide et de puzzle.


## I. Présentation des exercices avec IDE 

Vous trouverez dans les paragraphes suivants des modèles de syntaxes à copier. Il s'agit ici de présenter les différentes possibilités.

### Un exemple complet:

* Cet exemple a été paramétré pour qu'au bout de 2 essais validés infructueux la réponse s'affiche.
* Dès que l'élève a réussi (vérification dans le fichier `exo_test.py` caché), la solution s'affiche, ainsi que les remarques contenues dans le fichier `exo_REM.md`

???+ question

    La fonction `somme` prend en paramètre une liste de nombres et renvoie la somme des nombres de cette liste

    Compléter le script ci-dessous : 
    
    * ⚠️ **N'oubliez surtout pas** de valider ![valider](images/valider.png){ width=3% } **après** avoir exécuté ![play](images/play.png){ width=3% }

    * Dans cet exercice, vous avez droit à deux essais validés :  
    Si les assert "ne passent pas", vous pouvez cliquer sur `###` en haut à droite de la fenêtre avant de valider, pour que l'essai soit décompté. Au bout de deux essais validés, la solution s'affichera.


    {{IDE('scripts/somme', MAX = 2)}}

### Les syntaxes (Source Vincent Bouillot)

!!! danger "Attention"

    Les fichiers contenant le sujet, les tests, la correction et les remarques doivent se trouver dans un dossier `scripts` placé dans le même dossier que votre fichier `.md` courant.

    Les noms des fichiers doivent respecter la normalisation suivante : 

    👉 Pour un exercice Python dont le code à compléter est dans le fichier **mon_exo.py** :

    * Le fichier de correction doit être nommé : **mon_exo_corr.py**
    * Le fichier de tests doit être nommé : **mon_exo_test.py**
    * Le fichier de remarques (facultatif) doit être nommé : **mon_exo_REM.md** (Ne pas oublier les majuscules)

Les différentes syntaxes à utiliser sont présentées ci-dessous, dans les différents onglets. 

!!! summary "Les syntaxes"

    === "Terminal"
        ```markdown
        {% raw %}
        {{ terminal() }}
        {% endraw %}
        ```
        Création d'un terminal vide. L'auto-complétion avec ++tab++ et le rappel de l'historique (avec ++ctrl+"R"++ ) sont possibles.

        {{ terminal () }}

    === "IDE vide"
        ```markdown
        {% raw %}
        {{ IDE() }}
        {% endraw %}
        ```
        Création d'un IDE vide, visuellement proche de Thonny. La zone de saisie se redimensionne automatiquement et autorise l'auto-complétion de type _snippet_ avec ++alt+space++.

        {{IDE()}}

    === "IDE vertical"
        ```markdown
        {% raw %}
        {{ IDEv() }}
        {% endraw %}
        ```
        Cette commande crée un IDE vide, avec division verticale. 

        {{IDEv()}}

    === "IDE avec code"
        ```markdown
        {% raw %}
        {{ IDE('foo/bar/nom_de_fichier', MAX = 8, SANS = 'max,min') }}
        {% endraw %}
        ```
        
        - Le fichier `nom_de_fichier.py` est chargée dans un IDE. Ce fichier doit être situé impérativement dans `docs/scripts/foo/bar/`. 
    
        - `MAX = 8` : indique le nombre maximal de tentatives de validation que l'élève peut effectuer. `MAX = 1000` ou `MAX = "+"` permet de mettre ce nombre à l'infini. Valeur par défaut : `MAX = 5` .

        - `SANS = 'max,min'` permet d'interdire l'utilisation des fonctions built-ins `#!python max` et `#!python min`.

        Les IDE sont enregistrés à intervalle de temps régulier. Ils permettent également l'autocomplétion avec la combinaison de touches ++alt+space++.

        {{IDE('demo/demo1')}}

    === "IDE vertical avec code"
        ```markdown
        {% raw %}
        {{ IDEv('foo/bar/nom_de_fichier', MAX = 1000) }}
        {% endraw %}
        ```
        Cette commande charge le fichier `nom_de_fichier` dans un IDE avec division verticale. Le fichier doit être dans `docs/scripts/foo/bar/`.       

        {{IDEv('demo/demo2', MAX = 3)}}

## II. A savoir

!!! danger "Attention"

    Un exercice avec IDE doit absolument se trouver dans une admonition `???+ question` 


!!! danger "Attention"

    Il ne faut pas, dans la même admonition que celle qui contient un IDE, mettre une autre admonition (ni avant l'IDE, ni après l'IDE). L'IDE ne doit pas non plus se trouver dans une admonition imbriquée.

    Cela pertuberait entre autres, l'admonition suivante, et l'affichage des remarques après résolution de l'exercice.


## III. Exemple 1 avec un IDE, un fichier corr, un fichier test, mais pas de fichier REM

Par défaut les élèves peuvent valider leur exercice 5 fois avant que la correction ne s'affiche.

!!! info ""
    
    ````markdown title="Code à copier"
    ???+ question "Exercice 1"

        Compléter le script ci-dessous (observer les `assert`):
        {% raw %}
        {{IDE('scripts/bonjour')}}
        {% endraw %}
    ````

???+ question "Exercice 1"

    Compléter le script ci-dessous (observer les `assert`):

    {{IDE('scripts/bonjour')}}


!!! info "Fichiers utilisés pour cet exemple"

    ```python title="bonjour.py"
    def accueil(prenom):
        ...

    # Tests
    assert accueil("Alice") == "Bonjour Alice"
    assert accueil("Bob")  == "Bonjour Bob"
    ```

    ```python title="bonjour_corr.py"
    def accueil(prenom):
        return "Bonjour " + prenom
    ```

    ```python title="bonjour_test.py"
    # Tests
    assert accueil("Alice") == "Bonjour Alice"
    assert accueil("Bob")  == "Bonjour Bob"

    # Autres tests
    assert accueil("fegrehjtyjtqfqsgeryryrfg") == "Bonjour fegrehjtyjtqfqsgeryryrfg"
    ```


## IV. Exemple 2 avec un IDE, un fichier corr, un fichier test, mais pas de fichier REM

!!! info ""

    Dans cet exemple le fichier sujet en Python ne contient pas de ligne `# Tests`. Il n'y a pas d'essais décomptés dans ce cas-là, et l'élève peut ne jamais trouver la solution.


    ````markdown title="Code à copier"
    ???+ question "Exercice 2"

        Compléter le script ci-dessous :
        {% raw %}
        {{IDE('scripts/construction')}}
        {% endraw %}
    ````

???+ question "Exercice 2"

    Compléter le script ci-dessous :

    {{IDE('scripts/construction')}}


!!! info "Solution en clair mais repliée"

    On peut toujours mettre la solution "en clair" si on le désire, **mais absolument en dehors de l'admonition** `???+ question`

    ````markdown title="Code à copier"

    ??? success "Solution"

        ```python
        # un tableau cents en compréhension qui contient 10 entiers 100.
        cents = [100 for k in range(10)]

        # un tableau entiers en compréhension qui contient les 10 entiers entre 1 et 10 compris.
        entiers = [k for k in range(1, 11)]
        ```
    ````
    

    ??? success "Solution"

        ```python
        # un tableau cents en compréhension qui contient 10 entiers 100.
        cents = [100 for k in range(10)]

        # un tableau entiers en compréhension qui contient les 10 entiers entre 1 et 10 compris.
        entiers = [k for k in range(1, 11)]
        ```

!!! info "Fichiers utilisés pour cet exemple"

    ```python title="construction.py"
    # un tableau cents en compréhension qui contient 10 entiers 100.
    cents = ...

    # un tableau entiers en compréhension qui contient les 10 entiers entre 1 et 10 compris.
    entiers = ...
    
    ```

    ```python title="construction_corr.py"
    cents = [100 for k in range(10)]
    entiers = [k for k in range(1, 11)]

    ```

    ```python title="construction_test.py"
    # Tests
    assert cents == [100 for k in range(10)]
    assert entiers == [k for k in range(1, 11)]

    ```

## V. Exemple 3 avec un IDE, un fichier corr, un fichier test, et un fichier REM

Dans cet exemple on a utilisé la syntaxe : `MAX = 2`. Les élèves pourront valider leur exercice 2 fois avant que la correction ne s'affiche.

!!! warning "Le code à copier pour cet exemple"

    😊 Le code à copier n'a pas été oublié. Il se trouve après le rendu de l'exemple


???+ question "Exercice 3"

    * La fonction `est_pair` prend en paramètre un entier. 
    * Elle renvoie `True` s'il est pair, et `False` dans le cas contraire.

    Compléter le script ci-dessous : 

    N'oubliez pas de valider après avoir exécuté.
            
    {{IDE('scripts/pair', MAX = 2)}}


!!! info ""    
    ```markdown title="Code à copier"

    ???+ question "Exercice 3"

        * La fonction `est_pair` prend en paramètre un entier **différent de 0**. 
        * Elle renvoie `True` s'il est pair, et `False` dans le cas contraire.

        Compléter le script ci-dessous : 

        N'oubliez pas de valider après avoir exécuté.

        {% raw %}
        {{IDE('scripts/pair', MAX = 2)}}
        {% endraw %}
    ```

!!! info "Fichiers utilisés pour cet exemple"

    ```python title="pair.py"
    def est_pair(nombre):
    ...

    # Tests
    assert est_pair(2)
    assert est_pair(2000)
    assert not est_pair(1)
    assert not est_pair(777)
    
    ```

    ```python title="pair_corr.py"
    def est_pair(nombre):
    return nombre % 2 == 0
    
    ```

    ```python title="pair_test.py"
    # Tests
    assert est_pair(2)
    assert est_pair(2000)
    assert not est_pair(1)
    assert not est_pair(777)

    # Autres tests
    assert est_pair(10**10)
    assert not est_pair(10**10 + 1)

    ```

    ````markdown title="pair_REM.md"
    !!! info "Expression booléenne"

        Remarquons que `nombre % 2 == 0` est une expression booléenne qui s'évalue à `True` ou `False`

    Il est **très maladroit** d'écrire

    ```python
    def est_pair(nombre):
        if nombre % 2 == 0:
            return True
        else:
            return False
    ```
    ````

## VI. Exemple 4 avec des "interdits" Python

!!! info "SANS"

    Dans cet exercice, nous allons utiliser la syntaxe `SANS = "max, sorted, sort"` pour interdire l'utilisation de `max`, ainsi que de `sort` ou `sorted`.


!!! warning "Le code à copier pour cet exemple"

    😊 Le code à copier n'a pas été oublié. Il se trouve après le rendu de l'exemple


### Exercice : 

Écrire une fonction `maximum` :

- prenant en paramètre une liste **non vide** de nombres : `nombres`
- renvoyant le plus grand élément de cette liste.

Chacun des nombres utilisés est de type `int` ou `float`.

!!! danger "Contrainte"

    On interdit ici d'utiliser `max`, ainsi que `sort` ou `sorted`.

???+ example "Exemples"

    ```pycon
    >>> maximum([98, 12, 104, 23, 131, 9])
    131
    >>> maximum([-27, 24, -3, 15])
    24
    ```

???+ question "Exercice 4"

    Compléter ci-dessous 

    {{ IDE('scripts/maximum', SANS = "max, sorted, sort") }}


!!! info ""    

    ````markdown title="Code à copier"

    

    Écrire une fonction `maximum` :

    - prenant en paramètre une liste **non vide** de nombres : `nombres`
    - renvoyant le plus grand élément de cette liste.

    Chacun des nombres utilisés est de type `int` ou `float`.

    !!! danger "Contrainte"

        On interdit ici d'utiliser `max`, ainsi que `sort` ou `sorted`.

    ???+ example "Exemples"

        ```pycon
        >>> maximum([98, 12, 104, 23, 131, 9])
        131
        >>> maximum([-27, 24, -3, 15])
        24
        ```
    ???+ question "Exercice 4"

        Compléter ci-dessous 

        {% raw %}
        {{ IDE('scripts/maximum', SANS = "max, sorted, sort") }}
        {% endraw %}

    ````
!!! info "Fichiers utilisés pour cet exemple"

    ```python title="maximum.py"
    def maximum(nombres):
    ...

    # Tests
    assert maximum([98, 12, 104, 23, 131, 9]) == 131
    assert maximum([-27, 24, -3, 15]) == 24
    ```

    ```python title="maximum_corr.py"
    def maximum(nombres):
        maxi = nombres[0]
        for x in nombres:
            if x > maxi:
                maxi = x
        return maxi
    ```

    ```python title="maximum_test.py"
    # Tests
    assert maximum([98, 12, 104, 23, 131, 9]) == 131
    assert maximum([-27, 24, -3, 15]) == 24

    # Tests supplémentaires
    assert maximum([1, 2, 3, 4, 5]) == 5
    assert maximum([5, 4, 3, 2, 1]) == 5
    assert maximum([5, 5, 5]) == 5
    assert abs(maximum([5.01, 5.02, 5.0]) - 5.02) < 10**-6
    assert maximum([-5, -4, -3, -8, -6]) == -3
    assert maximum([1,2]) == 2
    ```

    ```markdown title="maximum_REM.md"
    !!! info "algorithme classique à connaître"

        Il s'agit d'une recherche de maximum classique. La liste étant non-vide, on initialise la variable `maxi` avec la première valeur de la liste.
    ```


## VII. Exemple 5 avec du code caché dans le sujet en Python

!!! info "Code caché"

    Dans cet exemple le fichier sujet en Python contient une partie qui n'est pas visible par l'élève.

    Ces lignes de codes sont située entre  `#--- HDR ---#` et `#--- HDR ---#`. 

    Voir plus bas les fichiers utilisés dans cet exemple


````markdown title="Code à copier"
???+ question "Exercice 5"

	Vous ignorez le rôle de la fonction `mystere`, et vous ne voyez pas son code.

	Vous allez donc utiliser la fonction `help` native en Python.

    Exécuter ci-dessous :

    {% raw %}
    {{IDE('scripts/mystere_code_cache')}}
    {% endraw %}
````

???+ question "Exercice 5"

	Vous ignorez le rôle de la fonction `mystere`, et vous ne voyez pas son code.

	Vous allez donc utiliser la fonction `help` native en Python.

    Exécuter ci-dessous :

    {{IDE('scripts/mystere_code_cache')}}


!!! info "Fichiers utilisés pour cet exemple"

    ```python title="mystere_code_cache.py"
	#--- HDR ---#

	def mystere(nbre):
    	"""
    	La fonction prend en paramètre un nombre entier.
    	Elle renvoie True si ce nombre est un multiple de 7, False sinon

    	>>> mystere(21)
    	True
    	>>> mystere(22)
    	False

    	"""
    	return nbre % 7 == 0

	#--- HDR ---#

	help(mystere)
    
    ```
### Variante de cet exercice en utilisant la console


````markdown title="Code à copier"
???+ question "Exercice 5"

	Vous ignorez le rôle de la fonction `mystere`, et vous ne voyez pas son code.

	Vous allez donc utiliser la fonction `help` native en Python.

    Exécuter ci-dessous, puis dans la console qui apparaît (il s'affiche : %Script exécuté), recopier, puis ensuite cliquer sur la touche entrer pour exécuter :

    ```python
    help
    ```

    {% raw %}
    {{IDE('scripts/mystere_code_cache_bis')}}
    {% endraw %}
````

???+ question "Exercice 5"

	Vous ignorez le rôle de la fonction `mystere`, et vous ne voyez pas son code.

	Vous allez donc utiliser la fonction `help` native en Python.

    Exécuter ci-dessous, puis dans la console qui apparaît (il s'affiche : %Script exécuté), recopier, puis ensuite cliquer sur la touche entrer pour exécuter :

    ```python
    help(mystere)
    ```

    {{IDE('scripts/mystere_code_cache_bis')}}
    

!!! info "Fichiers utilisés pour cet exemple"

    ```python title="mystere_code_cache_bis.py"
	#--- HDR ---#

	def mystere(nbre):
    	"""
    	La fonction prend en paramètre un nombre entier.
    	Elle renvoie True si ce nombre est un multiple de 7, False sinon

    	>>> mystere(21)
    	True
    	>>> mystere(22)
    	False

    	"""
    	return nbre % 7 == 0

	#--- HDR ---#

    # La fonction mystere est cachée.
    # A vous d'en découvrir les spécifications
   
    ```

## VIII. Exemple 6 avec une plus grande fenêtre pour l'IDE



!!! info "Modifier la taille de l'IDE"

    Parfois on voudrait que l'IDE affiche un plus grand nombre de lignes.

    Il suffit d'utiliser la syntaxe (pour 55 lignes par exemple) `IDE('exo', SIZE=55)`


!!! info ""
    
    ````markdown title="Code à copier"
    ???+ question "Exercice"

        Compléter le script ci-dessous :
        {% raw %}
        {{IDE('scripts/grand_IDE', SIZE=55)}}
        {% endraw %}
    ````

???+ question "Exercice"

    Compléter le script ci-dessous :

    {{IDE('scripts/grand_IDE', SIZE=55)}}



## IX. Les fichiers REM

???+ tip "A savoir"

    Il ne faut pas mettre de titres `#`, ni `##`, ni `###`, ni `####` dans un fichier REM. (Le fichier de remarque serait cliquable dans la barre de menu, avec potentiellement les explications en clair)
    
    On peut séparer les différents paragraphes en les mettant dans des admonitions distinctes.


!!! info  "Affichage du fichier REM"

    * Si le fichier de remarque s'affiche en dessous de l'exercice dès le début, c'est que l'IDE n'est pas dans une admoniton :  
     `???+ question`

    * Si le fichier de remarque ne s'affiche pas, c'est qu'il y a peut-être une autre admonition après l'IDE (par exemple une admonition `??? success "Solution"`) Il faudrait la sortir de l'admonition de l'exercice (supprimer l'indentation).


## X. Exercice avec la réponse à chercher soi-même (avec un lien interne)

!!! info "La réponse est à chercher en suivant un lien"

    ```markdown title="Code à copier"

    ???+ question 

        Comment fait-on une admonition "note pliée" ?

        ??? success "Solution"

            Vous pouvez trouver la réponse ici : [admonitions](../2_basique/2_page_basique.md)

            On indique juste le nom du fichier s'il est dans le même dossier.
    ```

    ???+ question 

        Comment fait-on une admonition "note pliée" ?

        ??? success "Solution"

            Vous pouvez trouver la réponse ici : [admonitions](../2_basique/2_page_basique.md)

            On indique juste le nom du fichier s'il est dans le même dossier.
        

## XI. Site de référence pour toutes les syntaxes

[Site de Vincent Bouillot](https://bouillotvincent.gitlab.io/pyodide-mkdocs/){ .md-button target="_blank" rel="noopener" }


## XII. Exemple  de puzzle

Site pour créer les puzzles : [Création de puzzles](https://www.codepuzzle.io/){ .md-button target="_blank" rel="noopener" }

Cliquer sur le menu en haut à droite (plier/déplier) pour recopier le code à insérer.

Rendu : 

La réponse attendue est `cinq = [5 for k in range(20)]`

???+ question
    
    <iframe src="https://www.codepuzzle.io/p/T3WK" width="100%" height="300" frameborder="0"></iframe>
    
