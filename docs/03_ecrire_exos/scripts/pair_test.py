# Tests
assert est_pair(2)
assert est_pair(2000)
assert not est_pair(1)
assert not est_pair(777)

# Autres tests
assert est_pair(10**10)
assert not est_pair(10**10 + 1)
assert est_pair(0)
