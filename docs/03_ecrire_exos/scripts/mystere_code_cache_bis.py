#--- HDR ---#

def mystere(nbre):
    """
    La fonction prend en paramètre un nombre entier.
    Elle renvoie True si ce nombre est un multiple de 7, False sinon

    >>> mystere(21)
    True
    >>> mystere(22)
    False

    """
    return nbre % 7 == 0

#--- HDR ---#

# La fonction mystere est cachée.
# A vous d'en découvrir les spécifications


