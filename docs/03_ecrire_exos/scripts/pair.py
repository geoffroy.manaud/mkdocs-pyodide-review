def est_pair(nombre):
    ...

# Tests
assert est_pair(2)
assert est_pair(2000)
assert not est_pair(1)
assert not est_pair(777)

