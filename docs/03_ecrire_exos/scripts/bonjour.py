def accueil(prenom):
    ...

# Tests
assert accueil("Alice") == "Bonjour Alice"
assert accueil("Bob")  == "Bonjour Bob"

