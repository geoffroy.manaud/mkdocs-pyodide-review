---
author: Mireille Coilhac et Vincent-Xavier Jumel
title: Complément - Notebooks avec admonitions
---

Nous allons voir comment créer des Notebook jupyter avec de jolies boites (admonitions)

Pour créer votre notebook, suivre ce lien dont l'URL contient `?extensions=admonitions`

[admonitions](https://notebook.basthon.fr/?extensions=admonitions ){ .md-button target="_blank" rel="noopener" }

Il y a beaucoup d'exemples ici : 
[modèle avec boites](https://notebook.basthon.fr/?extensions=admonitions&from=examples/python3-admonitions.ipynb){ .md-button target="_blank" rel="noopener" }



## I.  Solution à dérouler sur clic : 

````html title="Exemple"
<details><summary>Clic pour lire </summary>
    
+ Point 1  <br>
  
+ Point 2

</details>
````

<div class="result" html>
<details><summary>Clic pour lire </summary>
    
+ Point 1  <br>
   
+ Point 2

</details>
</div>


## II. Des titres avec du HTML



```html title="Un grand titre"
<span style="display:block;
        padding:5px;
        background-color:#afa;
        border:1px solid #bbffbb;
        border-radius:20px;
        font-weight:bold;
        font-size:2em;
        padding:15px;
        text-align:center;">
    Grand titre
</span>
```
<div class="result" markdown>
<span style="display:block;
        padding:5px;
        background-color:#afa;
        border:1px solid #bbffbb;
        border-radius:20px;
        font-weight:bold;
        font-size:2em;
        padding:15px;
        text-align:center;">
    Grand titre
</span>
</div>


```html title="Un plus petit titre"
<span style="margin:10px;
            padding:5px;
            background-color:#afa;
            border:1px solid #bbffbb;
            border-radius:20px;
            font-weight:bold;
            font-size:1.2em;
            text-align:center;">Plus petit titre en boite 
</span>
```
<div class="result" markdown>
<span style="margin:10px;
            padding:5px;
            background-color:#afa;
            border:1px solid #bbffbb;
            border-radius:20px;
            font-weight:bold;
            font-size:1.2em;
            text-align:center;">Plus petit titre en boite 
</span>
</div>

## III. Pour approfondir

La [documentation de référence](https://basthon.fr/theme/assets/pdf/Basthon_Documentation.pdf){ .md-button target="_blank" rel="noopener" }